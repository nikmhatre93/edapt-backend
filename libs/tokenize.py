import jwt
from app import app, logger


def createToken(payload):
    token = jwt.encode(payload, app.config.get(
        'SECRET_KEY'), algorithm='HS256')
    return token.decode('unicode_escape')


def getPayload(token):
    try:
        payload = jwt.decode(token, app.config.get(
            'SECRET_KEY'), algorithm='HS256')
    except jwt.ExpiredSignature:
        logger.error('Signature verification failed, Invalid Signature')
        payload = None
    except jwt.DecodeError:
        logger.error('Error decoding payload')
        payload = None

    return payload
