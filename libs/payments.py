from instamojo_wrapper import Instamojo
from app import app
import requests
import json


class PaymentInstamojo(object):
    """docstring for Payment"""

    def __init__(self):
        super(PaymentInstamojo, self).__init__()

    def convert_amount_to_inr(self, amount):
        response = requests.get(
            "http://openexchangerates.org/api/latest.json?app_id=e4bb785c32b843458d88d9bcb3e30661")
        response = json.loads(response.text)
        self.conversion_rate = response["rates"]["INR"] / \
            response["rates"]["CAD"]
        self.inr_amount = float(amount)*self.conversion_rate
        return str(round(self.inr_amount, 2))

    def payment_request(self, amount, purpose, convert_to_inr=True):
        api = Instamojo(api_key=app.config.get("INSTAMOJO_API_KEY"),
                        auth_token=app.config.get("INSTAMOJO_AUTH_TOKEN"),
                        endpoint=app.config.get('INSTAMOJO_URL'))

        if convert_to_inr:
            inr_amount = self.convert_amount_to_inr(amount)
        else:
            inr_amount = str(amount)

        # Create a new Payment Request
        response = api.payment_request_create(
            amount=inr_amount,
            purpose=purpose,
            redirect_url=app.config.get('URL')+"/payment/callback",
            allow_repeated_payments=False
        )
        return response, inr_amount

    def get_status(self, payment_id):

        api = Instamojo(api_key=app.config.get("INSTAMOJO_API_KEY"),
                        auth_token=app.config.get("INSTAMOJO_AUTH_TOKEN"),
                        endpoint=app.config.get('INSTAMOJO_URL'))

        response = api.payment_request_status(payment_id)
        return response
