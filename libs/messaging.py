#!/usr/bin/env python

import urllib
import json
from app import app, logger
import requests


class Messaging():
    """Handles the Messaging for Tweeny"""

    def sendOTP(self, country_code, number, code, first_name='User'):

        url = "http://api.smsala.com/api/SendSMS"

        request_body = {
            "api_id": app.config.get("SMSALA_APIID"),
            "api_password": app.config.get("SMSALA_APIPASS"),
            "phonenumber": str(country_code) + str(number),
            "sender_id": app.config.get("SMSALA_SENDERID"),
            "sms_type": "T",
            "encoding": "T",
            "templateid": None,
            "textmessage": "Hi {}, Thank you for joining Edapt!, Please use OTP {} to verify your mobile number.".format(first_name, code),
            "V1": None,
            "V2": None,
            "V3": None,
            "V4": None,
            "V5": None
        }

        api_response = requests.post(url, json=request_body)
        # response.content = b'{"message_id":46841,"status":"S","remarks":"Message Submitted Sucessfully"}'

        app.logger.error("send otp response: ", api_response.content.decode)

        return json.loads(api_response.content.decode())
