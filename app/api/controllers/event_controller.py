from flask import Flask, jsonify, Response, request
from flask_restful import Resource, Api, reqparse
from datetime import datetime, timedelta
import json
import uuid
from bson import json_util
from bson.objectid import ObjectId

from libs.decorators import *
from libs import tokenize
import conf.constants as C

from app import app, db, logger

from ..models.event_models import *
from .custom_utils import ResponseUtil


class KhEvent(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, event_id=None, **kwargs):
        if event_id:
            return Event().get_one(event_id)

        else:
            return Event().get_all()

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('EVENTMGR')])
    def post(self, event_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['user_id'] = kwargs['payload']['user_id']

            return Event().add_one(json_data)

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Invalid JSON request")

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('EVENTMGR')])
    def put(self, event_id=None, **kwargs):
        if not event_id:
            return ResponseUtil(False, 403).message_response("Invalid Event ID")

        try:
            json_data = request.get_json(force=True)
            # json_data['user_id'] = kwargs['payload']['user_id']
            json_data["event_id"] = event_id

            return Event()._update(json_data)

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Invalid JSON request")
