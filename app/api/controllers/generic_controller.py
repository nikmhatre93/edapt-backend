from flask import Flask, jsonify, Response, request, render_template, redirect, make_response
from datetime import datetime, timedelta
from flask_restful import Resource, Api, reqparse
import random
import json
import uuid
import base64
import string
import random
import urllib3
from app import app
from urllib import parse
import certifi
from .custom_utils import ResponseUtil, generate_otp
from ..models.user_models import User
import conf.constants as C
from libs.messaging import *
from libs.decorators import *
from libs.email_notification import *
import os
import time
import pymongo
from mongoengine import connect
from libs import tokenize, push_notification
from flask import abort
from copy import deepcopy


# import sendgrid
# from sendgrid.helpers.mail import *


@app.route('/countrycodes')
def get_country_codes():
    codes = []
    for key, value in C.COUNTRY_CODES.items():
        codes.append({
            "label": key + ' (+' + value + ')',
            "value": '+' + value
        })
    codes = get_sorted(codes, 'label')
    return ResponseUtil(True, 200).json_data_response({'country_codes': codes})

@app.route('/countries')
def get_all_country():
    codes = []
    for key, value in C.COUNTRY_CODES.items():
        codes.append({
            "label": key,
            "value": key
        })
    codes = get_sorted(codes, 'label')
    return ResponseUtil(True, 200).json_data_response({'countries': codes})

@app.route('/languages')
def get_languages():
    languages = []
    for language in C.LANGUAGES:
        languages.append({
            "label": language,
            "value": language
        })
    languages = get_sorted(languages, 'label')
    return ResponseUtil(True, 200).json_data_response({'languages': languages})


@app.route('/testimonials')
def get_testimonials():
    return ResponseUtil(True, 200).json_data_response({'testimonials': C.TESTIMONIALS})

def get_sorted(list_of_dict, var, reverse_sort=False):
    if var == "created":
        if reverse_sort:
            return reversed(sorted(list_of_dict, key=lambda k: k["created"]["$date"]))
        else:
            return sorted(list_of_dict, key=lambda k: k["created"]["$date"])
    else:
        return sorted(list_of_dict, key=lambda k: k[var])


def get_years_months():
    today = datetime.today()
    current_month, current_year = today.month, today.year
    ret_years, ret_months = [], {}
    for year in C.YEARS:
        if current_year < int(year):
            ret_years.append({"label": year, "value": year})
            ret_months[year] = [{"label": k, "value": k, "index": v}
                                for k, v in C.MONTHS.items()]
            ret_months[year] = get_sorted(ret_months[year], 'index')

        elif current_year == int(year):
            ret_years.append({"label": year, "value": year})
            ret_months[year] = [{"label": k, "value": k, "index": v}
                                for k, v in C.MONTHS.items() if C.MONTHS[k] >= current_month]
            ret_months[year] = get_sorted(ret_months[year], 'index')

    return ret_years, ret_months


@app.route('/dropdowninfo')
def get_drop_down_info():
    dict_provinces = get_label_value(C.PROVINCES)
    dict_cities = get_label_value(C.CITIES)
    years, months = get_years_months()
    return ResponseUtil(True, 200).json_data_response({'dropdowninfo': {
        "provinces": dict_provinces,
        "cities": dict_cities,
        "years": years,
        "months": months
    }})


def get_label_value(constant):
    dict_ = {}
    for key in constant.keys():
        provinces = []
        for province in constant[key]:
            provinces.append({
                "label": province,
                "value": province
            })
        dict_[key] = provinces

    return dict_


class MobileVerification(Resource):

    # send an otp to current user's mobile number
    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, number=None, **kwargs):

        if number:
            contact = number
        else:
            try:
                user_obj = User.objects.get(_id=kwargs["payload"]["user_id"])

                if user_obj.is_contact_verified:
                    return ResponseUtil(False, 403).message_response("Hey! Your Mobile number is already verified.")

                # country_code = user_obj.country_code
                country_code = user_obj.country_code
                contact = user_obj.contact

            except Exception as e:
                return ResponseUtil(False, 403).message_response("Invalid request. Exception: {}".format(e))

        # print("Sending SMS to {}".format(contact))
        
        try:
            first_name = user_obj.first_name
        except Exception as e:
            first_name = 'User'
        code = generate_otp()

        params = {
            "sms_otp": code,
            "otp_valid_till": datetime.utcnow() + timedelta(minutes=10)
        }

        User.objects(_id=kwargs["payload"]["user_id"]).update_one(**params)

        send_otp_response = Messaging().sendOTP(country_code, contact, code, first_name)
        # {"message_id":46841,"status":"S","remarks":"Message Submitted Sucessfully"}

        # app.logger.error("send otp response ", send_otp_response)

        if send_otp_response.get("status") == "S":
            return ResponseUtil(True, 200).message_response("OTP has been sent to your contact number.")
        elif send_otp_response.get("status") == "F":
            return ResponseUtil(False, 403).message_response("Invalid contact number! Please check the digits again.")
        else:
            app.logger.error(send_otp_response)
            return ResponseUtil(False, 403).message_response("Internal Server Error")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, number=None, **kwargs):
        try:
            json_data = request.get_json(force=True)

            try:
                user_obj = User.objects.get(_id=kwargs["payload"]["user_id"])

                if user_obj.is_contact_verified:
                    return ResponseUtil(False, 403).message_response("Hey! Your Mobile number is already verified.")

                if user_obj.sms_otp == json_data["code"]:
                    if user_obj.otp_valid_till >= datetime.utcnow():
                        user_obj.is_contact_verified = True
                        user_obj.save()
                        return ResponseUtil(True, 200).message_response("Your contact number has been verified.")
                    else:
                        return ResponseUtil(False, 403).message_response(
                            "This OTP is Expired. Please request a new one")
                else:
                    return ResponseUtil(False, 403).message_response("Incorrect OTP, please check the digits.")

            except Exception as e:
                return ResponseUtil(False, 403).message_response("Exception: {}".format(e))

        except Exception as e:
            return ResponseUtil(False, 403).json_data_response("Failed! Exception: {}".format(e))


def generate_token(username):
    app.logger.error("Generate token called {}".format(username))
    payload = {
        "email": username,
        "timestamp": time.time()
    }
    app.logger.error(payload)
    return tokenize.createToken(payload)


def get_user_time(token):
    payload = tokenize.getPayload(token)
    app.logger.error("Getting tokens...........")
    app.logger.error(payload)
    email = payload["email"]
    timestamp = payload["timestamp"]
    return email, timestamp


class ForgotPassword(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data["email"]
        try:
            user_obj = User.objects.get(email=email)
            if not user_obj:
                return ResponseUtil(False, 403).message_response("Could not find the linked user account")
            
            email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES["forgotpassword"]))

            token = generate_token(email)
            user_obj.change_password_token = token
            user_obj.save()
            
            email_obj.template['message'] = email_obj.template['message'].format(
                FIRST_NAME=user_obj['first_name'], TOKEN=token)

            email_params = {
                "to_email": email
            }
            email_response = email_obj.send_mail(email_params)
            del email_obj
            if email_response["status"]:
                return email_response
            else:
                return ResponseUtil(False, 403).message_response("Failed to send email")
        except Exception as e:
            return ResponseUtil(False, 403).message_response("Error {}".format(e))


class ForgotChangePassword(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        
        try:
            token = json_data["token"]
            password = json_data["password"]
            email, timestamp = get_user_time(token)
            User.objects(email=email).update(
                password=json_data["password"])

            return ResponseUtil(True, 200).message_response("Password Sucessfully updated")
            
        except Exception as e:
            return ResponseUtil(False, 403).message_response("Error Invalid token/params {}".format(e))


@app.route('/user/account/verify/<token>', methods=['GET'])
def UserAccountVerify(token):
    if( 'favicon' in token):
        abort(404)
    app.logger.error("Entered here")
    message = "Please login to the App and start greeting local experts and fellow aspirants"
    app.logger.error("Verifying account")
    try:
        print("inside try")
        email, timestamp = get_user_time(token)
        app.logger.error(email)
        print("before db calls")
        User.objects(email=email).update(is_active=True)
        print("after deb <calls></calls>")
        
    except Exception as e:
        print(e)
        app.logger.error(e)
        message = e
    return render_template('account_verified.html', message=message)
        

@app.route('/onboarding/<no>', methods=['GET'])
def onboarding(no):
    link = 'static/image/bg_onboarding_'+no+'.jpg'
    return ResponseUtil(True, 200).json_data_response({'link': link})


@app.route('/checkstatus')
def check_status():
    auth_header = request.headers.get('Authorization')
    if auth_header == C.UPDOWN_TOKEN:
        try:
            db = connect(app.config.get('MONGODB_DATABASE'), username=app.config.get(
                'MONGODB_USERNAME'), password=app.config.get('MONGODB_PASSWORD'),
                ServerSelectionTimeoutMS=1000)
            dbnames = db.database_names()
            return ResponseUtil(True, 200).json_message_response("Working!")

        except pymongo.errors.ServerSelectionTimeoutError as err:
            return ResponseUtil(False, 403).json_message_response("Not Working!")
    else:
        return ResponseUtil(False, 403).json_message_response("Not Authorised")


