from flask import Flask, jsonify, Response, request, render_template, redirect, make_response, abort
from flask_restful import Resource, Api, reqparse
from datetime import datetime, timedelta
import json
import uuid
from bson import json_util
from bson.objectid import ObjectId

from libs.decorators import *
from libs import tokenize
import conf.constants as C

from app import app, db, logger

from ..models.user_models import *
from ..models.event_models import *
from ..models.service_models import *
from .custom_utils import ResponseUtil, get_sorted
from libs.email_notification import *
from copy import deepcopy


@app.route('/')
def landing():
    return render_template('login.html')
    # return json.dumps({"message": "Hello from the root",
    #                    "app": "KnowHassels v1"})


class AdminLogin(Resource):

    def post(self):
        print("admin login")
        try:
            json_data = request.get_json(force=True)

            json_data["login_as"] = "admin"

            admin_login_response = User().validate_user(json_data)

            print(admin_login_response)
            return admin_login_response

        except Exception as e:
            print("admin login exception {}".format(e))
            return ResponseUtil(False, 403).message_response("Exception: {}".format(e))


def getDashboardStats():

    total_students = User.objects(roles="kh_student").count()
    total_buddies = User.objects(roles="kh_buddy").count()
    total_feeds = TimelineFeed.objects.count()
    total_events = Event.objects.count()

    return {
        'total_students': total_students,
        'total_buddies': total_buddies,
        'total_feeds': total_feeds,
        'total_events': total_events
    }


@app.route('/admin/dashboard')
@allowed_page_roles(['kh_admin'])
def admin_dashboard(**kwargs):

    payload = kwargs["payload"]

    if C.APP_ROLES.get("ADMIN") in payload['role']:
        stats = getDashboardStats()

        return render_template('dashboard_admin.html', data={
            'user': kwargs['payload'],
            'stats': stats
        }, isPage="dashboard")
    else:
        abort(404)


@app.route('/admin/dashboard/feed', methods=["GET"])
@allowed_page_roles(['kh_admin'])
def admin_dashboard_feed(**kwargs):

    params = {}

    user_feeds_response = TimelineFeed().get_all(
        params, order_by="created", reverse_sort=True)

    if user_feeds_response["status"]:
        user_feeds = user_feeds_response["data"]
    else:
        user_feeds = []

    # print(user_feeds)

    return render_template('feeds.html', feeds=user_feeds, isPage="feeds")


@app.route('/admin/dashboard/student', methods=["GET"])
@allowed_page_roles(['kh_admin'])
def admin_dashboard_student(**kwargs):

    get_students_response = Student().get_all(
        order_by="created", reverse_sort=True)

    # print(get_students_response)

    if get_students_response['status']:
        students = get_students_response["data"]["students"]
    else:
        students = []

    # for student in students:
    #     print(student, "\n\n")

    return render_template('students.html', students=students, isPage="students")


@app.route('/admin/dashboard/buddy', methods=["GET"])
@allowed_page_roles(['kh_admin'])
def admin_dashboard_buddy(**kwargs):

    get_buddies_response = Buddy().get_all(
        service_model=Service, order_by="created", reverse_sort=True)

    if get_buddies_response['status']:
        buddies = get_buddies_response["data"]["buddies"]
    else:
        buddies = []

    # for buddy in buddies:
    #     print(buddy, "\n\n")

    return render_template('buddies.html', buddies=buddies, isPage="buddies")


@app.route('/admin/dashboard/transaction', methods=["GET"])
@allowed_page_roles(['kh_admin'])
def admin_dashboard_transaction(**kwargs):

    get_transaction_response = Transaction().get_all(None, None)

    print(get_transaction_response)

    if get_transaction_response['status']:
        transactions = get_transaction_response["data"]["transactions"]
    else:
        transactions = []

    for transaction in transactions:
        if 'payment_to_edapt_time' not in transaction or transaction['payment_to_edapt_time'] == None:
            transaction["payment_to_edapt_time"] = transaction["created"]

        student_obj = User.objects.get(_id=transaction["student_id"])

        buddy_obj = User.objects.get(_id=transaction["buddy_id"])

        transaction["student_name"] = " ".join(
            [student_obj.first_name, student_obj.last_name])
        transaction["buddy_name"] = " ".join(
            [buddy_obj.first_name, buddy_obj.last_name])

        transaction["currently_hired"] = False
        transaction["services_completion"] = 0

        if Student.objects.get(user_id=transaction["student_id"]).hired_buddy == transaction["buddy_id"]:
            transaction["currently_hired"] = True

            service_task_response = ServiceTask().get_all({
                "student_id": transaction["student_id"],
                "buddy_id": transaction["buddy_id"]
            })

            if service_task_response["status"]:
                transaction["services_completion"] = service_task_response["data"]["percentage"]

    transactions = get_sorted(
        transactions, "payment_to_edapt_time", reverse_sort=True)

    print(transactions)

    return render_template('transactions.html', transactions=transactions, isPage="transactions")


@app.route('/admin/dashboard/issue', methods=["GET"])
@allowed_page_roles(['kh_admin'])
def admin_dashboard_issue(**kwargs):

    issues = json.loads(
        ReportIssue.objects.all().to_json())

    issues = get_sorted(issues, "created", reverse_sort=True)

    for issue_data in issues:
        student_obj = User.objects.get(_id=issue_data["student_id"])
        buddy_obj = User.objects.get(_id=issue_data["buddy_id"])
        issue_data["student_name"] = " ".join(
            [student_obj.first_name, student_obj.last_name])
        issue_data["buddy_name"] = " ".join(
            [buddy_obj.first_name, buddy_obj.last_name])

    return render_template('issues.html', issues=issues, isPage="issues")


class KhUser(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def get(self, user_id=None, **kwargs):

        try:
            print(kwargs['payload'])

            # print(user_id)

            if user_id:
                return User().get_one(user_id)
            else:
                return User().get_all()  # ResponseUtil(False, 403).message_response("User ID required")

        except Exception as e:
            print("admin - get user Exception: {}".format(e))
            ResponseUtil(False, 403).message_response("Exception {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def post(self, user_id=None, **kwargs):
        if user_id == None:
            return ResponseUtil(False, 403).message_response("Invalid request. User ID required.")

        try:
            json_data = request.get_json(force=True)

            if "is_verified" in json_data:
                params = {
                    "user_id": user_id,
                    "is_verified": json_data["is_verified"]
                }
                return Buddy()._update(params)

            if "is_active" in json_data:
                params = {
                    "user_id": user_id,
                    "is_active": json_data["is_active"]
                }

                # send email notification to buddy
                try:
                    buddy_obj = User.objects.get(_id=user_id)
                    # send activated email
                    if json_data["is_active"]:
                        email_obj = Email_(
                            deepcopy(C.EMAIL_TEMPLATES['account_activated_buddy']))
                    # send deactivated email
                    else:
                        email_obj = Email_(
                            deepcopy(C.EMAIL_TEMPLATES['account_deactivated_buddy']))

                    format_data = {
                        "MENTOR_NAME": buddy_obj.first_name
                    }

                    email_params = {
                        "to_email": buddy_obj.email
                    }

                    email_obj.template['message'] = email_obj.template['message'].format(
                        **format_data)

                    email_response = email_obj.send_mail(email_params)
                    del email_obj
                    app.logger.error(
                        "account activated/deactivated email response {}".format(str(email_response)))

                except Exception as e:
                    app.logger.error(
                        'failed to send account activation email to buddy error {}'.format(str(e)))

                return User().update_user_meta(params)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    # change role from student to buddy
    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('ADMIN')])
    def put(self, user_id=None, **kwargs):
        try:
            if C.APP_ROLES.get('STUDENT') in kwargs['payload']['role']:
                return User().update_role_to_buddy(user_id=kwargs['payload']['user_id'])
            else:
                if user_id:
                    return User().update_role_to_buddy(user_id=user_id)
                else:
                    return ResponseUtil(False, 403).message_response("Invalid User ID")
        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Exception: " + str(e))


class AdminFeed(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def get(self, **kwargs):
        try:
            args = request.args.to_dict()

            user_id = request.args.get("user_id")
            visible = request.args.get("visible")
            params = {}

            if user_id:
                params["user_id"] = user_id
            if visible == "true":
                params["visible"] = True
            if visible == "false":
                params["visible"] = False

            print(params)
            feeds = TimelineFeed().get_all(params)

            return feeds

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Exception: " + str(e))

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            feed_id = json_data["feed_id"]
            visible = json_data["visible"]
            return TimelineFeed().change_visible(feed_id, visible)

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Exception: " + str(e))


class AdminMessage(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def get(self, **kwargs):
        try:
            args = request.args.to_dict()

            if all(x in args.keys() for x in ["user_id1", "user_id2"]):

                params = {"sender_id__in": [args['user_id2'], args['user_id1']],
                          "receiver_id__in": [args['user_id2'], args['user_id1']]}

                user_messages = Message().get_message_admin(params)

            else:

                user_messages = []

            return ResponseUtil(False, 403).json_data_response({"messages": user_messages})

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Exception: " + str(e))


class AdminTransaction(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def get(self, **kwargs):
        try:
            args = request.args.to_dict()
            transaction_id = args.get('transaction_id')
            buddy_paid = args.get('buddy_paid')
            status_to_kh = args.get('status_to_kh')

            if buddy_paid:
                buddy_paid = bool(int(buddy_paid))

            if status_to_kh:
                status_to_kh = bool(int(status_to_kh))

            if transaction_id:
                response = Transaction().get_one(transaction_id)
                return response

            else:
                response = Transaction().get_all(buddy_paid, status_to_kh)
                return response

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Exception {}".format(e))

    # @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    # def post(self, **kwargs):
    #     try:


class AdminIssue(Resource):

    # @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    # def get(self, **kwargs):
    #     try:
    #         issues = json.loads(ReportIssue.objects().to_json())
    #         return ResponseUtil(True, 200).data_response(issues)

    #     except Exception as e:
    #         return ResponseUtil(False, 403).message_response("Exception {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            issue_id = json_data["issue_id"]
            issue_obj = ReportIssue.objects.get(_id=issue_id)

            if "remove_hired_buddy" in json_data and json_data["remove_hired_buddy"]:
                student_id = issue_obj.student_id
                Student.objects(user_id=str(student_id)).update(
                    hired_buddy=None, student_availability=[], buddy_availability=[])

                deleted = ServiceTask.objects(
                    student_id=str(student_id)).delete()
                issue_obj.remove_hired_buddy = json_data["remove_hired_buddy"]

                # Send a push notification to the student
                try:
                    registration_id = User.objects.get(
                        _id=student_id).fcm_token

                    if registration_id:
                        student_name = User.objects.get(
                            _id=student_id).first_name
                        display_response = PushNotification(registration_id).send_display_message({
                            "title": "Mentor Removed",
                            'message': "Hey! {}, Your current mentor has been removed. You can now hire another mentor".format(student_name)
                            # "message": json.dumps({
                            #     'type': 'MENTOR_REMOVED',
                            #     'message': "Hey! {}, Your current mentor has been removed. You can now hire another mentor".format(student_name)
                            # })
                        })

                        if display_response["success"] != 1:
                            app.logger.error(
                                "Failed to send push notification")

                except Exception as e:
                    app.logger.error(
                        "Exception while sending push notification: {}".format(e))

            if "issue_resolved" in json_data:
                issue_obj.issue_resolved = json_data["issue_resolved"]

            if "remarks" in json_data:
                issue_obj.remarks = json_data["remarks"]
            issue_obj.updated = datetime.datetime.utcnow()
            issue_obj.save()

            return ResponseUtil(True, 200).message_response("Updated Successfully")

        except Exception as e:
            app.logger.error(
                "Failed to update for issue_id {}, exception {}".format(issue_id, e))
            return ResponseUtil(False, 403).message_response("Failed to update, {}".format(e))
