from flask import Flask, jsonify, Response, request
from flask_restful import Resource, Api, reqparse
from datetime import datetime, timedelta
import json
import uuid
from bson import json_util
from bson.objectid import ObjectId

from libs.decorators import *
from libs import tokenize
import conf.constants as C

from app import app, db, logger

from ..models.service_models import Service, Package, ServiceTask
from ..models.user_models import Buddy, Student
from .custom_utils import ResponseUtil


class KhServiceDefault(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def post(self, **kwargs):

        return Service().add_default()

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def get(self, **kwargs):

        return Service().get_default()

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def put(self, **kwargs):

        try:
            json_data = request.get_json(force=True)

            valid_fields = ["service_id", "name", "details"]

            if all(x in valid_fields for x in json_data.keys()):

                return Service()._update(json_data)

            else:

                return ResponseUtil(False, 403).message_response("Invalid fields")

        except Exception as e:

            return ResponseUtil(False, 403).message_response("Invalid json request")


class BuddyPackage(Resource):

    @allowed_api_roles([C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('ADMIN')])
    def get(self, buddy_id=None, **kwargs):
        if buddy_id:
            # get buddy package info
            try:
                package_id = Buddy.objects.get(user_id=buddy_id).package_id
            except Exception as e:
                return ResponseUtil(False, 403).message_response("Invalid Buddy ID")

            return Package().get_one(package_id)

        else:
            return ResponseUtil(False, 403).message_response("buddy_id required")

    @allowed_api_roles([C.APP_ROLES.get('BUDDY')])
    def post(self, buddy_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)

            if "package_id" in json_data:

                try:
                    self_obj = Buddy.objects.get(
                        user_id=kwargs["payload"]["user_id"])

                    package_ids = []
                    for package_id in json_data["package_id"]:
                        if Package.objects(_id=package_id):
                            package_ids.append(package_id)

                    self_obj.package_id = package_ids
                    self_obj.is_package_selected = True
                    self_obj.save()
                    return ResponseUtil(True, 200).message_response("Package Selected Successfully")

                except Exception as e:
                    return ResponseUtil(False, 403).message_response("Exception: {}".format(e))

            else:
                return ResponseUtil(False, 403).message_response("Package ID required")

        except Exception as e:

            return ResponseUtil(False, 403).message_response("Invalid json request")


class ServicePackage(Resource):

    @allowed_api_roles([C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('ADMIN')])
    def get(self, pkg_id=None, **kwargs):
        if pkg_id:
            return Package().get_one(pkg_id)
        else:
            return Package().get_all()

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def post(self, pkg_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)

            return Package().add_one(json_data)

        except Exception as e:

            return ResponseUtil(False, 403).message_response("Invalid json request, Exception: {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def put(self, pkg_id=None, **kwargs):
        if pkg_id:
            # update package details

            pass
        else:
            return ResponseUtil(False, 403).message_response("package_id required")


class KhServiceTask(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, task_id=None, **kwargs):
        # get service task list and completion status according to login_as
        # role
        if kwargs["payload"]["login_as"].upper() == "BUDDY":
            # get service task based on buddy_id and student_id
            url_params = request.args.to_dict()

            if "student_id" in url_params and url_params["student_id"]:

                if not task_id:
                    try:
                        params = {
                            "student_id": url_params["student_id"],
                            "buddy_id": kwargs["payload"]["user_id"]
                        }

                        return ServiceTask().get_all(params)

                    except Exception as e:

                        return ResponseUtil(False, 403).message_response("Exception: {}".format(e))

                else:
                    ResponseUtil(False, 403).message_response(
                        "Can't get response for single service_task")

            else:
                ResponseUtil(False, 403).message_response(
                    "student_id missing in url arguments")

        elif kwargs["payload"]["login_as"].upper() == "STUDENT":
            # get service task based on student id
            if not task_id:
                try:
                    params = {
                        "student_id": kwargs["payload"]["user_id"],
                        "buddy_id": Student.objects.get(user_id=kwargs["payload"]["user_id"]).hired_buddy
                    }

                    if params["buddy_id"]:

                        return ServiceTask().get_all(params)

                    else:
                        return ResponseUtil(False, 403).message_response("Buddy ID invalid")

                except Exception as e:

                    return ResponseUtil(False, 403).message_response(
                        "Exception while getting service tasks: {}".format(e))

            else:
                ResponseUtil(False, 403).message_response(
                    "Can't get response for single service_task")

        else:
            return ResponseUtil(False, 403).message_response("Could not identify current login role")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, task_id=None, **kwargs):
        # update a service task according to login_as role

        try:
            json_data = request.get_json(force=True)

            if all(x in json_data for x in ["task_id", "completed"]):

                if kwargs["payload"]["login_as"].upper() == "BUDDY":
                    # update checked_by_buddy for a task
                    params = {
                        "task_id": json_data["task_id"],
                        "checked_by_buddy": json_data["completed"]
                    }

                    return ServiceTask()._update(params)

                elif kwargs["payload"]["login_as"].upper() == "STUDENT":
                    # update checked_by_student for a task
                    params = {
                        "task_id": json_data["task_id"],
                        "checked_by_student": json_data["completed"]
                    }

                    return ServiceTask()._update(params)

                else:
                    return ResponseUtil(False, 403).message_response(
                        "Could not identify current login role or invalid task_id")

            else:
                return ResponseUtil(False, 403).message_response("Incomplete request fields")

        except Exception as e:

            return ResponseUtil(False, 403).message_response("Invalid json request, Exception: {}".format(e))


class add_new(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        return Package().add_new(json_data)


class get_all_services(Resource):
    def get(self):
        return Service().get_all_services()


class update_package(Resource):
    def put(self):
        json_data = request.get_json(force=True)
        return Package().update_package(json_data)

class get_buddy_packages(Resource):
    def get(self, buddy_id=None):
        return Package().get_buddy_packages(buddy_id)