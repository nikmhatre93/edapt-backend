# import all the controllers explicitly
# from .controllers import login
from .controllers import admin_controller, user_controller, event_controller, service_controller

# import all the models explicitly

from app import app
from flask_restful import Api

api = Api(app)

# import all the controllers explicitly
from .controllers import generic_controller

# import all the models explicitly
from .models import user_models

# from .models import admin_models

# All Routes can be declared in this file itself since it imports all the controllers

api.add_resource(user_controller.UserLogin, '/login')
api.add_resource(user_controller.UserRegister, '/register')
api.add_resource(user_controller.UserAccount, '/account', '/account/<pic>')
api.add_resource(user_controller.UpdatePassword, '/user/pwd')
api.add_resource(user_controller.FCMUpdate, '/fcm/update')

api.add_resource(user_controller.KhStudent, '/student', '/student/<user_id>')
api.add_resource(user_controller.KhStudents, '/students', '/student/<user_id>')
# api.add_resource(user_controller.KhStudentsCount, '/studentsCount/<city>/<province>')

api.add_resource(user_controller.KhBuddy, '/buddy', '/buddy/<user_id>')
api.add_resource(user_controller.KhBuddies, '/buddies', '/buddies/<user_id>')

api.add_resource(user_controller.does_exists, '/does_exists') # new api's

api.add_resource(user_controller.BuddyVerify,
                 '/buddy/verify/<doc_type>', '/buddy/verify/<doc_type>/<user_id>')


api.add_resource(user_controller.BuddyBank,
                 '/buddy/bank', '/buddy/bank/<buddy_id>')

api.add_resource(user_controller.Bookmark,
                 '/bookmark/profile', '/bookmark/profile/<role>')

api.add_resource(user_controller.UserBuddy, '/mybuddy')

api.add_resource(user_controller.AvailableDates,
                 '/availabledates', '/availabledates/<student_id>')

api.add_resource(service_controller.ServicePackage,
                 '/package', '/package/<pkg_id>')

api.add_resource(service_controller.add_new, '/addNew') # new api's

api.add_resource(service_controller.update_package, '/update_package') # new api's

api.add_resource(service_controller.get_buddy_packages, '/get_buddy_packages/<buddy_id>') # new api's

api.add_resource(service_controller.BuddyPackage,
                 '/package/buddy', '/package/buddy/<buddy_id>')

api.add_resource(service_controller.KhServiceTask,
                 '/service/task', '/service/task/<task_id>')

api.add_resource(service_controller.get_all_services, '/get_all_services')

api.add_resource(generic_controller.MobileVerification,
                 '/verify/mobile/<number>', '/verify/mobile')

api.add_resource(generic_controller.ForgotPassword,
                 '/password/forgot')
api.add_resource(generic_controller.ForgotChangePassword,
                 '/password/change')


# add default services to buddy
api.add_resource(service_controller.KhServiceDefault, '/service/default')

api.add_resource(user_controller.Dashboard, '/dashboard')

api.add_resource(user_controller.UserFeed, '/feed')

api.add_resource(user_controller.Messages,
                 '/message', '/message/<receiver_id>')

api.add_resource(event_controller.KhEvent, '/event', '/event/<event_id>')
api.add_resource(user_controller.ChatList, '/chat')

api.add_resource(user_controller.DeleteJunk, '/deletejunk')
api.add_resource(user_controller.AddDefaults, '/adddefault')

# admin routes
api.add_resource(admin_controller.AdminLogin, '/admin/login')
api.add_resource(admin_controller.KhUser, '/admin/user/<user_id>')
api.add_resource(admin_controller.AdminFeed, '/admin/feed')
api.add_resource(admin_controller.AdminMessage, '/admin/message')
api.add_resource(admin_controller.AdminIssue, '/admin/issue')


# api.add_resource(generic_controller.CountryCode, '/countrycodes')
# api.add_resource(user_controller.ImageUrl, '/imgurlfix')

api.add_resource(user_controller.Payment, '/payment', '/payment/<action>')
api.add_resource(admin_controller.AdminTransaction, '/admin/transaction')
api.add_resource(user_controller.StudentTravel, '/student/traveldoc',
                 '/student/traveldoc/<doc_type>', '/student/traveldoc/<doc_type>/<user_id>')
