from mongoengine import Document, fields
from ..controllers.custom_utils import ResponseUtil, get_random_code
import datetime
from libs import tokenize
import conf.constants as C
from bson import ObjectId
import json
import urllib.parse
from app import app


class Event(Document):
    _id = fields.ObjectIdField()
    name = fields.StringField()
    details = fields.StringField()
    image = fields.StringField()
    event_dt = fields.DateTimeField()
    user_id = fields.StringField()

    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "event"}

    def get_one(self, event_id=None):
        if event_id:
            try:
                event_obj = self.__class__.objects.get(_id=event_id)

                response_data = {
                    "name": event_obj.name,
                    "details": event_obj.details,
                    "event_dt": event_obj.event_dt.ctime()
                }

                return ResponseUtil(True, 200).data_response(response_data)
            except Exception as e:
                return ResponseUtil(False, 403).message_response("Invalid Event ID")
        else:
            return ResponseUtil(False, 404).message_response("Event ID missing")

    def get_all(self):
        try:
            events = json.loads(self.__class__.objects().only(
                "_id", "name", "details", "created", "updated").to_json())

            if events:
                return ResponseUtil(True, 200).json_data_response({
                    "events": events
                })
            else:
                return ResponseUtil(False, 404).message_response("No events found.")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response(str(e))

    def add_one(self, params):
        try:
            if self.__class__.objects(user_id=params["user_id"], name=params["name"]):
                return ResponseUtil(False, 403).message_response("Event with same name already exists")
            else:
                try:
                    self.__class__.objects.create(
                        name=params["name"],
                        details=params["details"],
                        user_id=params["user_id"],
                        image=params["image"],
                        event_dt=params["event_dt"]
                    )

                    return ResponseUtil(True, 200).message_response("Event added successfully")
                except Exception as e:
                    print(e)
                    return ResponseUtil(False, 403).message_response("Failed to add an event")

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Failed to add an Event. Exception: " + str(e))

    def _update(self, params=None):
        try:
            event_id = params.pop("event_id")
            params["updated"] = datetime.datetime.utcnow()

            updated = self.__class__.objects(_id=event_id).update_one(**params)

            if not updated:
                return ResponseUtil(False, 404).message_response("Invalid Event Id")
            else:
                return ResponseUtil(True, 200).message_response("Event updated successfully")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 404).message_response("Failed to update event. Exception: "+str(e))


class Subscriber(Document):
    _id = fields.ObjectIdField()
    event_id = fields.StringField()
    user_id = fields.StringField()

    created = fields.DateTimeField()
    updated = fields.DateTimeField()
