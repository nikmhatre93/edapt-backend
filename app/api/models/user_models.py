from mongoengine import Document, fields
from ..controllers.custom_utils import ResponseUtil, get_random_code, get_sorted
from libs.push_notification import *
import datetime
from libs import tokenize
import conf.constants as C
from bson import ObjectId
import json
import urllib.parse
from app import app
import uuid
import os
import base64
from PIL import Image
from io import BytesIO
import requests
from libs.payments import *


class User(Document):
    _id = fields.ObjectIdField()
    username = fields.StringField(required=True)
    password = fields.StringField(required=True)
    first_name = fields.StringField(required=True)
    last_name = fields.StringField(required=True)
    sex = fields.StringField(default="")
    about = fields.StringField(default="")
    country_code = fields.StringField(required=True, default="")
    contact = fields.StringField(required=True)
    email = fields.StringField(default="")
    sms_otp = fields.IntField(default=None)
    otp_valid_till = fields.DateTimeField()
    home_address = fields.StringField(default="")
    city = fields.StringField(default="")
    origin = fields.StringField(default="")
    country = fields.StringField(default="")
    university = fields.StringField(default="")
    date_of_birth = fields.DynamicField(default="")
    mother_tongue = fields.StringField(default="")
    languages = fields.ListField(default=[])
    image = fields.StringField(
        default="/static/image/default_user.png")
    education = fields.StringField(default="")
    employment = fields.StringField(default="")
    token = fields.StringField(required=True)
    login_type = fields.StringField(required=True)
    is_buddy = fields.BooleanField(default=False)
    is_active = fields.BooleanField(default=False)
    is_deleted = fields.BooleanField(default=False)
    is_contact_verified = fields.BooleanField(default=False)
    roles = fields.ListField(fields.StringField())
    created = fields.DateTimeField()
    updated = fields.DateTimeField()
    home_town = fields.StringField(default="")
    current_city = fields.StringField(default="")
    current_country = fields.StringField(default="")
    change_password_token = fields.StringField(default="")
    fcm_token = fields.StringField(default="")
    age = fields.IntField(default=None)
    interests = fields.ListField(default=[])
    lastSeen = fields.DynamicField(default="")
    deviceType = fields.StringField(default='')
    notification = fields.BooleanField(default=True)

    meta = {"collection": "users"}

    def get_all(self, params=None):
        if not params:
            users = json.loads(self.__class__.objects(**params).only(
                *C.USER_INFO_FIELDS + ["roles", "is_active"]).to_json())

        else:
            users = []

        return ResponseUtil(True, 200).json_data_response({
            "users": users
        })

    def get_percentage(self, data, login_as=None):
        try:
            # print(data)
            count = 0
            expected_values = [None, "", []]
            for req_key in C.REQUIRED_USER_ATTRIBUTES:
                if req_key in data and data[req_key] not in expected_values:
                    count += 1
            percentage = int((count * 100) / len(C.REQUIRED_USER_ATTRIBUTES))

            # if login_as == "STUDENT":
            #     for req_key in C.REQUIRED_TRAVELLING_ATTRIBUTES:
            #         if req_key in data and data[req_key] not in expected_values:
            #             count += 1

            #     percentage = int(
            #         (count * 100) / (len(C.REQUIRED_TRAVELLING_ATTRIBUTES) + len(C.REQUIRED_USER_ATTRIBUTES)))

            # elif login_as == "BUDDY":
            #     for req_key in C.REQUIRED_SERVICING_ATTRIBUTES:
            #         if req_key in data and data[req_key] not in expected_values:
            #             count += 1
            #     percentage = int(
            #         (count * 100) / (len(C.REQUIRED_SERVICING_ATTRIBUTES) + len(C.REQUIRED_USER_ATTRIBUTES)))

            return ResponseUtil(True, 200).data_response({"percentage": percentage})

        except Exception as e:
            app.logger.error(
                "Exception while getting percentage: {}".format(e))
            return ResponseUtil(False, 403).message_response({"message": e})

    def get_one(self, user_id, service_task_model=None):
        try:
            obj = json.loads(self.__class__.objects(_id=user_id).only(
                *C.USER_INFO_FIELDS).to_json())

            if obj:

                user_info = obj[0]

                # print(user_info)

                if "$date" in user_info["date_of_birth"]:
                    user_info["date_of_birth"] = str(
                        user_info["date_of_birth"]["$date"])
                else:
                    user_info["date_of_birth"] = ""

                if C.APP_ROLES.get('STUDENT') in user_info["roles"]:

                    student_travelling_info = Student(
                    ).get_travelling_info(user_info["_id"]["$oid"], service_task_model=service_task_model)

                    # app.logger.error(student_travelling_info)

                    if student_travelling_info["status"]:
                        user_info.update(student_travelling_info["data"])

                        try:
                            hired_buddy = Student.objects.get(
                                user_id=user_id).hired_buddy
                        except Exception as e:
                            print(
                                "Could not get buddy_id from Student document. Exception {}".format(e))
                            hired_buddy = None

                        user_info["buddy_id"] = hired_buddy
                    else:
                        user_info["buddy_id"] = None

                    # print("updated user info", user_info)

                elif C.APP_ROLES.get('BUDDY') in user_info["roles"]:

                    buddy_servicing_info = Buddy(
                    ).get_servicing_info(user_info["_id"]["$oid"])

                    print('here', buddy_servicing_info)
                    if buddy_servicing_info["status"]:
                        user_info.update(buddy_servicing_info["data"])

                return ResponseUtil(True, 200).data_response(user_info)
            else:
                raise Exception("Could not find the user.")
        except Exception as e:
            message = "Exception {}".format(e)
            return ResponseUtil(False, 403).message_response(message)

    def update_password(self, params):
        try:
            # check if the user is valid user
            try:
                user = self.__class__.objects.get(
                    username=params['username'], password=params['old_password'])
                if not user:
                    return ResponseUtil(False, 403).json_message_response("Ohh no! You entered an incorrect password. Please try again!")
            except Exception as e:
                raise e

            obj = self.__class__.objects(_id=ObjectId(params['user_id'])).update_one(
                set__password=params['new_password'], set__first_time_login=False)
            # print(obj)

            return ResponseUtil(True, 200).json_message_response("Hey! Your password has been updated.")
        except Exception as e:
            message = "Invalid credentials {}".format(e)
            return ResponseUtil(False, 403).json_message_response(message)

    def validate_user(self, params):

        # app.logger.error("User Login Params: "+str(params))

        try:
            user_obj = self.__class__.objects.get(
                username=params['username'])

            if user_obj.password == params['password']:

                # print(user_obj.is_active)
                # prevent user from logging in if is_active is false
                if user_obj.is_deleted:
                    return ResponseUtil(False, 403).json_message_response(
                        "This profile has been deleted. Contact admin@khedapt.com to know more!")

                if not user_obj.is_active:
                    return ResponseUtil(False, 403).json_message_response(
                        "You are almost there! Please verify your email address and start exploring. please contact the admin@khedapt.com If your account is still deactivated.")

                payload = {
                    "first_name": user_obj.first_name,
                    "last_name": user_obj.last_name,
                    "username": user_obj.username,
                    "user_id": str(user_obj._id),
                    "role": user_obj.roles,
                    "email": user_obj.email
                }

                if "login_as" in params and str(params["login_as"]).upper() in C.APP_ROLES:

                    if C.APP_ROLES.get(str(params["login_as"]).upper()) in user_obj.roles:
                        payload["login_as"] = str(params["login_as"]).upper()
                    else:
                        return ResponseUtil(False, 403).message_response("Failed to login")
                else:
                    return ResponseUtil(False, 403).message_response("Invalid Request. login_as field missing")

                # add hired buddy id if role == "student"

                if C.APP_ROLES.get("STUDENT") in payload["role"]:
                    try:
                        payload["buddy_id"] = Student.objects.get(
                            user_id=payload["user_id"]).hired_buddy
                    except Exception as e:
                        print(e)
                        payload["buddy_id"] = None

                if params["login_as"].upper() == "BUDDY":
                    try:
                        buddy_obj = Buddy.objects.get(
                            user_id=str(user_obj._id))
                        payload["package_id"] = buddy_obj.package_id
                        payload["is_verified"] = buddy_obj.is_verified

                    except Exception as e:
                        print(e)
                        payload["package_id"] = ""
                        payload["is_verified"] = False

                # app.logger.error(params)
                # app.logger.error(payload)

                access_token = tokenize.createToken(payload)
                # check if the travelling info or service info updated
                try:
                    if params['login_as'].upper() == 'STUDENT':
                        payload['info_exist'] = Student.objects.get(
                            user_id=payload['user_id']).info_exists

                    elif params['login_as'].upper() == 'BUDDY':
                        payload['info_exist'] = Buddy.objects.get(
                            user_id=payload['user_id']).info_exists

                except Exception as e:
                    payload["info_exist"] = False

                payload['login_type'] = user_obj.login_type

                return ResponseUtil(True, 200).json_data_response({
                    "user": payload,
                    "access_token": access_token
                })

            else:
                return ResponseUtil(False, 403).message_response("Incorrect password")

        except Exception as e:
            app.logger.error("validate user exception {}".format(e))
            return ResponseUtil(False, 403).message_response("Please enter valid email to continue")

    def does_exists(self, email):
        try:
            user = self.__class__.objects.get(username=email)
            # print(user)

            if user:
                return ResponseUtil(True, 200).data_response({
                    "email": user.username,
                    "is_buddy":user.is_buddy,
                    "id": str(user._id)
                })
            else:
                raise Exception("User email not present make a new one")
        except self.__class__.MultipleObjectsReturned as e:
            return ResponseUtil(True, 200).message_response("User email already taken")

        except Exception as e:
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

    def become_mentor(self, user_id, json_data):
        try:
            usr_obj = self.__class__.objects(_id=user_id)
        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).json_message_response("User does not exist")
        
        if 'kh_buddy' in usr_obj[0].roles:
            return ResponseUtil(False, 403).json_message_response("Already Registered as Mentor")
        else:
            try:
                username = usr_obj[0].username
                password = usr_obj[0].password
                
                json_data['roles'] = usr_obj[0].roles + ['kh_buddy']
                usr_obj.update(**json_data)
                
                Buddy().add_default(user_id)

                return self.validate_user({
                    'username': username,
                    'password': password,
                    'login_type' : 'knowhassels',
                    'login_as': 'BUDDY'
                })
            except Exception as e:
                app.logger.error(e)
                return ResponseUtil(False, 403).json_message_response("something went wrong!")


    def create_one(self, params, buddy_register=False):
        '''
            This function will add a new user <student, buddy> based on the buddy_register
            Add a user to the db with the login_type and token fields
        '''

        # print("\n\n", "buddy_register", buddy_register, "\n\n")

        # app.logger.error("User Registration Params: "+str(params))

        # check if the email already exists
        response = self.does_exists(params['username'])

        # print(response)

        # login type for google and facebook
        external = ['google', 'facebook']
        if params['login_type'].lower() in external:
            if self.__class__.objects(username=params['username']):
                if self.__class__.objects(username=params['username'], login_type=params['login_type']):
                    params["login_as"] = params["role_type"]
                    external_response = self.validate_user(params)
                    return external_response

                else:
                    return ResponseUtil(False, 403).json_message_response("Invalid login type")
            try:
                facebook_image = requests.get(params["image"])
                image_name = '.'.join([str(uuid.uuid4()), 'jpg'])
                filename = os.path.join(
                    app.config['IMAGE_UPLOAD_FOLDER'], image_name)

                with open(filename, "wb") as f:
                    f.write(facebook_image.content)

                params["image"] = "/".join(["/static/image", image_name])
            except Exception as e:
                app.logger.error(
                    "error while getting image from external source: {}".format(e))
                pass

        if not buddy_register:

            if response['status']:
                return ResponseUtil(False, 403).json_message_response("Ahh! This email address is already registered")

            if params['role_type'].upper() not in C.APP_ROLES.keys():
                return ResponseUtil(False, 403).json_message_response("Invalid login type <student, buddy>")

        elif self.__class__.objects(username=params['username'], is_buddy=True):
            return ResponseUtil(False, 403).message_response("You are already registered as Mentor")

        else:
            pass

        try:
            # if True:
            if buddy_register:
                is_buddy = True

                if response["status"] and C.APP_ROLES.get("STUDENT") in self.__class__.objects.get(
                        username=params["username"]).roles:
                    user_roles = [C.APP_ROLES.get(
                        "STUDENT"), C.APP_ROLES.get("BUDDY")]
                else:
                    user_roles = [C.APP_ROLES.get(
                        str(params['role_type']).upper())]

            else:
                is_buddy = False
                user_roles = [C.APP_ROLES.get(
                    str(params['role_type']).upper())]

            # print(params)
            if "email" not in params:
                if "@" in params["username"]:
                    params["email"] = params["username"]
                else:
                    params["email"] = ""

            try:
                if len(str(params["contact"])) and self.__class__.objects(username__ne=params["username"], contact=params["contact"]):
                    return ResponseUtil(False, 403).message_response("Contact number already exists. Please use a different contact number")

                user_creation_params = {
                    "username": str(params["username"]),
                    "email": str(params["email"]),
                    "password": str(params['password']),
                    "login_type": params['login_type'],
                    "first_name": params['first_name'],
                    "last_name": params['last_name'],
                    "country_code": params['country_code'],
                    "contact": str(params['contact']),
                    "token": params['token'],
                    "sex": params['sex'] if 'sex' in params else "",
                    "origin": params['origin'] if 'origin' in params else ""
                }

                app.logger.error(user_creation_params)

                if "image" in params:
                    user_creation_params["image"] = params["image"]
                else:
                    user_creation_params["image"] = "/static/image/default_user.png"

                self.__class__.objects.create(**user_creation_params)

            except Exception as e:
                app.logger.error("user_creation_params: " +
                                 str(user_creation_params))
                app.logger.error("Exception while creating user {}".format(e))

            try:
                params["is_buddy"] = is_buddy
                # params["upsert"] = True
                params["roles"] = user_roles
                params["created"] = datetime.datetime.utcnow()
                params["updated"] = datetime.datetime.utcnow()
                params["is_active"] = True
                params["is_deleted"] = False

                if params['login_type'] == "knowhassles":
                    params['is_active'] = False

                role_type = params.pop('role_type')

                obj = self.__class__.objects(
                    username=params["username"]).update(**params)
                # first_name=params['first_name'],
                # last_name=params['last_name'],
                # password=params['password'],
                # contact=params['contact'],
                # email=params['email'],
                # token=params['token'],
                # login_type=params['login_type'],
                # is_active=True,
                # is_buddy=is_buddy,
                # roles=user_roles,
                # created=datetime.datetime.utcnow(),
                # updated=datetime.datetime.utcnow(),
                # upsert=True
                # )
            except Exception as e:
                ResponseUtil(False, 403).message_response(
                    "Exception {}".format(e))

            if "image" in params:
                self.__class__.objects(username=params["username"]).update_one(
                    image=params["image"])

            # add default data to buddy document
            if buddy_register:
                try:
                    Buddy().add_default(
                        str(self.__class__.objects.get(username=params["username"])._id))
                except Exception as e:
                    app.logger.error(
                        "Exception while adding default buddy info: {}".format(e))

            else:
                try:
                    print('here')
                    Student().add_default(
                        str(self.__class__.objects.get(username=params["username"])._id))
                except Exception as e:
                    app.logger.error(
                        "Exception while adding student buddy info: {}".format(e))

            # print(params)
            params["login_as"] = role_type
            login_response = self.validate_user(params)
            return login_response

        except Exception as e:
            message = "Invalid fields {}".format(e)
            return ResponseUtil(False, 403).json_message_response(message)

    def update_user_meta(self, params):
        try:
            user_id = params.pop("user_id")

            if all(x in C.USER_UPDATE_FIELDS for x in params):
                params["updated"] = datetime.datetime.utcnow()

                try:
                    self_obj = self.__class__.objects.get(_id=user_id)

                    if self_obj.login_type == "knowhassels" and "email" in params:
                        params["username"] = params["email"]

                except Exception as e:
                    app.logger.error(
                        "Exception while updating user info: {}".format(e))
                    return ResponseUtil(False, 403).message_response("User does not exists")

                try:
                    params["date_of_birth"] = datetime.datetime.strptime(
                        params["date_of_birth"], '%Y-%m-%d')
                except Exception as e:
                    params["date_of_birth"] = ""

                if "contact" in params:
                    if params["contact"] not in ["", "", "0"]:
                        if self.__class__.objects(_id__ne=user_id, contact=params["contact"]):
                            return ResponseUtil(False, 403).message_response("Contact number already exists. Please use a different contact number")
                        else:
                            if not self.__class__.objects.get(_id=user_id).contact == params["contact"]:
                                params["is_contact_verified"] = False

                updated = self.__class__.objects(
                    _id=user_id).update_one(**params)

                if "university" in params:
                    Student.objects(user_id=user_id).update_one(
                        university=params["university"]
                    )

                if updated:
                    return ResponseUtil(True, 200).message_response("Profile updated")
                else:
                    return ResponseUtil(False, 403).message_response("Failed to update")

            else:

                invalid_fields = [
                    x for x in params if x not in C.USER_UPDATE_FIELDS]

                return ResponseUtil(False, 403).message_response("Invalid fields: " + ", ".join(invalid_fields))

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Failed to update User Info. Exception: " + str(e))

    def update_user_profile_picture(self, files, user_id):

        try:
            if "image" in files:
                image = Image.open(files["image"])
                image.thumbnail((200, 200))
                ext = files["image"].filename.split('.')[1]
                image_name = '.'.join([str(uuid.uuid4()), ext])
                filename = os.path.join(
                    app.config['IMAGE_UPLOAD_FOLDER'], image_name)
                image.save(filename)
                params = {"image": '/'.join(["/static/image", image_name])}
                self.__class__.objects(_id=user_id).update(**params)
                return ResponseUtil(True, 200).message_response("Updated successfully")

            else:
                return ResponseUtil(False, 403).message_response("No image uploaded")

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Failed to update user profile picture. Exception: " + str(e))

    def update_active_status(self, user_id, params):
        try:
            is_active = None

            if 'is_active' in params:
                is_active = params['is_active']

            if is_active != None:

                is_updated = self.__class__.objects(
                    _id=user_id).update_one(set__is_active=is_active)
                # print(is_updated)
                if is_updated:
                    return ResponseUtil(True, 200).message_response("User active status is udpated successfully")
                else:
                    raise Exception("Could not update the active status")

            else:
                raise Exception("Cloud not update the active status")
        except Exception as e:
            message = "Exception {}".format(e)
            return ResponseUtil(False, 403).message_response(message)

    def update_role_to_buddy(self, user_id, params=None):
        user = self.__class__.objects(_id=user_id)

        if user and C.APP_ROLES["STUDENT"] in user.first().roles:
            roles = list(set([C.APP_ROLES["BUDDY"]] + user.first().roles))

            updated = user.update_one(roles=roles)

            if updated:
                return ResponseUtil(True, 200).message_response("User updated role successfully")
            else:
                return ResponseUtil(False, 403).message_response("Failed to update user role")
        else:
            return ResponseUtil(False, 404).message_response("User does not exists")

    def update_password(self, user_id, params):
        try:
            user = self.__class__.objects.get(_id=user_id)
        except Exception as e:
            return ResponseUtil(False, 404).message_response("User does not exists")
        
        if user.password == params['old_password']:
            user.password = params['new_password']
            user.save()

            return ResponseUtil(True, 200).message_response("Password changed successfully")
        else:
            return ResponseUtil(False, 403).message_response("Ohh no! You have entered an incorrect password. Please try again!")


class Student(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()

    arrival_city = fields.StringField(default="")
    arrival_country = fields.StringField(default="")
    arrival_province = fields.StringField(default="")

    air_ticket = fields.StringField(default="")
    passport_img = fields.StringField(default="")
    visa_img = fields.StringField(default="")
    arrival_dt = fields.DateTimeField(default="")
    arrival_terminal = fields.StringField(default="")
    departure_dt = fields.DateTimeField(default="")
    departure_terminal = fields.StringField(default="")
    flight_number = fields.StringField(default="")

    hired_buddy = fields.StringField(default=None)

    info_exists = fields.BooleanField(default=False)
    student_availability = fields.ListField(fields.StringField(), default=[])
    buddy_availability = fields.ListField(fields.StringField(), default=[])

    bookmarked_buddies = fields.ListField(fields.StringField())
    bookmarked_students = fields.ListField(fields.StringField())

    services = fields.ListField(fields.StringField())

    university = fields.StringField(default="")
    visible = fields.BooleanField(default=True)
    created = fields.DateTimeField()
    updated = fields.DateTimeField()
    travelling_month = fields.StringField(default="")
    travelling_year = fields.StringField(default="")

    meta = {"collection": "student"}

    def add_default(self, user_id):
        if not self.__class__.objects(user_id=user_id):

            utc_now = datetime.datetime.utcnow()

            self.__class__.objects.create(
                user_id=user_id,
                # service_start=datetime.datetime.strptime("Jan 2018", "%b %Y"),
                # service_end=datetime.datetime.strptime("Jan 2019", "%b %Y"),
                arrival_city="",
                arrival_country="",
                arrival_province="",
                air_ticket="",
                passport_img="",
                visa_img="",
                arrival_dt=utc_now,
                arrival_terminal="",
                departure_dt=utc_now,
                departure_terminal="",
                hired_buddy=None,
                bookmarked_buddies=[],
                bookmarked_students=[],
                services=[],
                university="",
                created=utc_now,
                updated=utc_now,
                travelling_month="",
                travelling_year="",
                info_exists=False,
                visible=True,
                student_availability=[],
                buddy_availability=[],
                flight_number=""
            )

        else:
            return ResponseUtil(False, 403).message_response("Student already exists")

    def get_travelling_info(self, user_id=None, service_task_model=None):
        try:
            student_obj = json.loads(self.__class__.objects(user_id=user_id)
                                     .only(*C.STUDENT_INFO_FOR_STUDENT).to_json())[0]

            if student_obj.get("arrival_dt"):
                datetime_obj = datetime.datetime.fromtimestamp(
                    student_obj["arrival_dt"]["$date"]/1000)
                days = (datetime_obj - datetime.datetime.utcnow()).days
                if days > 0:
                    student_obj["arrival_days_remaining"] = days
                else:
                    student_obj["arrival_days_remaining"] = 0

            else:
                student_obj["arrival_days_remaining"] = 0

            return ResponseUtil(True, 200).data_response(student_obj)

        except Exception as e:
            app.logger.error(
                "exception while getting travelling info for student: {}".format(e))
            return ResponseUtil(False, 403).message_response("User did not updated travelling details yet")

    def get_all(self, n=None, filters=None, self_id=None, self_role=None, order_by=None, reverse_sort=False, search_keyword=None):

        # if self_role == "buddy":
        #     # return list of students travelling where buddy serve
        #     try:
        #         buddy_obj = Buddy.objects.get(user_id=self_id)

        #         # first search students within same city
        #         query_params = {
        #             "arrival_city": buddy_obj.service_at_city,
        #             "arrival_province": self_obj.arrival_province,
        #             "arrival_country": buddy_obj.service_at_country,
        #             "travelling_month": buddy_obj.service_month,
        #             "travelling_year": buddy_obj.service_year
        #         }

        #         students = json.loads(self.__class__.objects(
        #             **query_params
        #         ).only(*C.STUDENT_INFO_FOR_BUDDY).to_json())

        #         # if students not found, search within same province
        #         if not students:
        #             query_params.pop("arrival_city")

        #             students = json.loads(self.__class__.objects(
        #                 **query_params
        #             ).only(*C.STUDENT_INFO_FOR_BUDDY).to_json())

        #     except Exception as e:
        #         print(e)
        # return ResponseUtil(False, 404).message_response("Please update your
        # servicing info first")

        if self_role == "student":
            # return list of students travelling along current student
            try:
                self_obj = self.__class__.objects.get(user_id=self_id)
                
                # print(json.loads(self_obj.to_json()))

                if not self_obj.visible:
                    return ResponseUtil(False, 403).message_response("Student not visible")

                # first search students within same city
                query_params = {
                    "user_id__ne": str(self_obj.user_id),
                    "arrival_city": self_obj.arrival_city,
                    "arrival_province": self_obj.arrival_province,
                    "arrival_country": self_obj.arrival_country,
                    "travelling_month": self_obj.travelling_month,
                    "travelling_year": self_obj.travelling_year,
                    "visible": True
                    # "university": self_obj.university
                }

                # filter results and show only currently active students
                active_users_oids = json.loads(User.objects(
                        is_active=True).filter(is_deleted=False).only("_id").to_json())

                active_user_str_ids = []

                for oid in active_users_oids:
                    active_user_str_ids.append(oid["_id"]["$oid"])

                query_params["user_id__in"] = active_user_str_ids

                # print(query_params)

                # arrival_country=self_obj.arrival_country,
                # arrival_dt__lte=self_obj.arrival_dt,
                # departure_dt__gte=self_obj.arrival_dt +
                # datetime.timedelta(days=30)

                students = json.loads(self.__class__.objects(
                    **query_params
                ).only(*C.STUDENT_INFO_FOR_STUDENT).to_json())

                # if students not found, search within same province
                if not students:
                    query_params.pop("arrival_city")

                    students = json.loads(self.__class__.objects(
                        **query_params
                    ).only(*C.STUDENT_INFO_FOR_STUDENT).to_json())

            except Exception as e:
                app.logger.error(
                    "Exception while getting list of co travellers: {}".format(e))
                return ResponseUtil(False, 404).message_response("Please update your travelling info first")

        else:
            students = json.loads(self.__class__.objects.all().to_json())

        # add meta info to student

        if students:

            students = self.get_students_meta_info(
                students, self_id, self_role, search_keyword)

            if order_by:
                students = get_sorted(students, order_by, reverse_sort)

            if n:
                students = students[:n]

            return ResponseUtil(True, 200).data_response({"students": students})
        else:
            return ResponseUtil(False, 404).message_response("Unfortunately, There are no co-travelers available. Please check again after sometime.")


    # def getCount(self, city=None, province=None):
    #     count = self.__class__.objects.filter(arrival_city=city).filter(visible=True).count() 
    #     if (count < 5):
    #         count = self.__class__.objects.filter(arrival_province=province).filter(visible=True).count() 
    #     return count
    

    def get_alls(self, n=None, filters=None, self_id=None, self_role=None, order_by=None, reverse_sort=False, search_keyword=None, offset=None):

        if self_role == "student":
            # return list of students travelling along current student
            try:
                self_obj = self.__class__.objects.get(user_id=self_id)
                
                # print(json.loads(self_obj.to_json()))

                if not self_obj.visible:
                    return ResponseUtil(False, 403).message_response("Student not visible")

                # first search students within same city
                query_params = {
                    "user_id__ne": str(self_obj.user_id),
                    "arrival_city": self_obj.arrival_city,
                    # "arrival_province": self_obj.arrival_province,
                    "arrival_country": self_obj.arrival_country,
                    "travelling_month": self_obj.travelling_month,
                    "travelling_year": self_obj.travelling_year,
                    "visible": True
                    # "university": self_obj.university
                }

                # query_params = {
                #     "user_id__ne": str(self_obj.user_id),
                #     "arrival_city": self_obj.arrival_city,
                #     # "arrival_province": self_obj.arrival_province,
                #     "arrival_country": self_obj.arrival_country,
                #     "travelling_month": self_obj.travelling_month,
                #     "travelling_year": self_obj.travelling_year,
                #     "visible": True
                #     # "university": self_obj.university
                # }

                # filter results and show only currently active students
                active_users_oids = json.loads(User.objects(
                        is_active=True).filter(is_deleted=False).only("_id").to_json())

                active_user_str_ids = []

                for oid in active_users_oids:
                    active_user_str_ids.append(oid["_id"]["$oid"])

                query_params["user_id__in"] = active_user_str_ids

                # print(query_params)

                # arrival_country=self_obj.arrival_country,
                # arrival_dt__lte=self_obj.arrival_dt,
                # departure_dt__gte=self_obj.arrival_dt +
                # datetime.timedelta(days=30)
                offset = int(offset)
                students = json.loads(self.__class__.objects(
                    **query_params
                ).order_by('-created').limit(n).skip(offset).only(*C.STUDENT_INFO_FOR_STUDENT).to_json())

                if offset == 0:
                    if len(students) < 5:
                        query_params.pop("arrival_city")
                        query_params['arrival_province'] = self_obj.arrival_province
                        students = json.loads(self.__class__.objects(
                            **query_params
                            ).order_by('-created').limit(n).skip(offset).only(*C.STUDENT_INFO_FOR_STUDENT).to_json())

                count = len(self.__class__.objects(**query_params).filter(visible=True))

                if offset == 0:
                    if count < 5:
                        query_params.pop("arrival_city")
                        query_params['arrival_province'] = self_obj.arrival_province
                        count = len(self.__class__.objects(**query_params).filter(visible=True))

                # if students not found, search within same province
                if offset == 0:
                    if not students:
                        query_params.pop("arrival_city")

                        students = json.loads(self.__class__.objects(
                            **query_params
                        ).order_by('-created').filter(visible=True).limit(n).skip(offset).only(*C.STUDENT_INFO_FOR_STUDENT).to_json())
                        
                        count = len(self.__class__.objects(**query_params).filter(visible=True))
                        
                        if count < 5:
                            query_params.pop("arrival_city")
                            query_params['arrival_province'] = self_obj.arrival_province
                            count = len(self.__class__.objects(**query_params).filter(visible=True))

            except Exception as e:
                app.logger.error(
                    "Exception while getting list of co travellers: {}".format(e))
                return ResponseUtil(False, 404).message_response("Please update your travelling info first")

        else:
            self_obj = Buddy.objects.get(user_id=self_id)
            offset = int(offset)
            query_params = {
                    "user_id__ne": str(self_id),
                    "arrival_city": self_obj.service_at_city,
                    # "arrival_province": self_obj.service_at_province,
                    "arrival_country": self_obj.service_at_country,
                    "travelling_month__in": self_obj.service_month,
                    "travelling_year__in": self_obj.service_year,
                    "visible": True
                    # "university": self_obj.university
                }
            students = json.loads(self.__class__.objects(**query_params).order_by('-created').filter(visible=True).limit(n).skip(offset).to_json())
            count = len(self.__class__.objects(**query_params).order_by('-created').filter(visible=True))
            # print('count =>',count)

            if offset == 0:
                if len(students) < 5:
                    query_params.pop('arrival_city')
                    query_params['arrival_province'] = self_obj.service_at_province
                    students = json.loads(self.__class__.objects(**query_params).order_by('-created').filter(visible=True).limit(n).skip(offset).to_json())
            
            
            if count < 5:
                # query_params.pop('arrival_city')
                query_params['arrival_province'] = self_obj.service_at_province
                count = len(self.__class__.objects(**query_params).order_by('-created').filter(visible=True))


        # add meta info to student

        if students:
            students = self.get_students_meta_info(
                students, self_id, self_role, search_keyword)

            if order_by:
                students = get_sorted(students, order_by, reverse_sort)
            
            return ResponseUtil(True, 200).data_response({"students": students, 'count': count})
        else:
            return ResponseUtil(False, 404).message_response("Unfortunately, There are no co-travelers available. Please check again after sometime.")



    def get_students_meta_info(self, students, self_id, self_role, search_keyword=None):

        try:
            if self_role == "student":
                bookmarked_students = self.__class__.objects.get(
                    user_id=self_id).bookmarked_students
            else:
                bookmarked_students = []
        except Exception as e:
            print("Exception: " + str(e))
            bookmarked_students = []
        filter_students = []
        for student in students:
            try:
                # print(student)
                user_obj = json.loads(User.objects(
                    _id=student["user_id"]).exclude("password").to_json())

                student.update(user_obj[0])

                # student["_id"] = {"$oid":   str(user_obj._id)}
                # student["image"] = user_obj.image
                # # student["full_name"] = " ".join(
                # #     [user_obj.first_name, user_obj.last_name])
                # student["first_name"] = user_obj.first_name
                # student["last_name"] = user_obj.last_name
                # student["city"] = user_obj.city
                # student["country_code"] = user_obj.country_code
                # student["country"] = user_obj.country
                # student["sex"] = user_obj.sex
                # student["about"] = user_obj.about
                # student["contact"] = user_obj.contact
                # student["email"] = user_obj.email
                # student["home_address"] = user_obj.home_address
                # student["origin"] = user_obj.origin
                # student["university"] = user_obj.university
                # student["date_of_birth"] = user_obj.date_of_birth if user_obj.date_of_birth == "" else str(
                #     int(user_obj.date_of_birth.timestamp()))
                # student["mother_tongue"] = user_obj.mother_tongue
                # student["languages"] = user_obj.languages
                # student["education"] = user_obj.education
                # student["employment"] = user_obj.employment
                # student["employment"] = user_obj.employment
                # student["is_buddy"] = user_obj.is_buddy

                if student["user_id"] in bookmarked_students:
                    student["bookmarked"] = True
                else:
                    student["bookmarked"] = False

                student.pop("user_id")

                if search_keyword:
                    keyword = search_keyword.lower()
                    if keyword in student['university'].lower() or keyword in student['first_name'].lower() or keyword in student['last_name'].lower():
                        filter_students.append(student)


            except Exception as e:
                print(e)
        if search_keyword:
            return filter_students
        return students

    def get_dashboard_data(self, self_id):
        try:
            user_info_response = User().get_one(self_id)

            if user_info_response["status"]:

                response_data = {
                    "user": user_info_response["data"]
                }

                return ResponseUtil(True, 200).data_response(response_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("User does not Exists")

    def _update(self, params=None):
        try:

            print(params)

            user_id = params.pop("user_id")

            if not User.objects(_id=user_id):
                return ResponseUtil(False, 404).message_response("User does not exists")
            else:
                utc_now = datetime.datetime.utcnow()

                if "arrival_dt" in params and "departure_dt" in params:

                    try:

                        params["arrival_dt"] = datetime.datetime.strptime(
                            params["arrival_dt"], "%Y-%m-%d %H:%M")
                        params["departure_dt"] = datetime.datetime.strptime(
                            params["departure_dt"], "%Y-%m-%d %H:%M")

                    except Exception as e:
                        print(
                            "Exception while updating travelling info: {}".format(e))
                        return ResponseUtil(False, 403).message_response(
                            "Could not update user info. Invalid Datetime format. Exception: {}".format(e))

                if not self.__class__.objects(user_id=user_id):
                    params["created"] = utc_now
                    params["upsert"] = True

                params["updated"] = utc_now

                if "university" in params:
                    response = User().update_user_meta({"user_id": user_id,
                                                        "university": params["university"]})

                if any(x in params for x in ["travelling_month", "travelling_year"]):
                    user_obj = self.__class__.objects.get(user_id=user_id)
                    hired_buddy = user_obj.hired_buddy
                    if hired_buddy:
                        buddy_obj = Buddy.objects.get(user_id=hired_buddy)
                        if params['travelling_month'] not in buddy_obj.service_month or params['travelling_year'] not in buddy_obj.service_year:
                            return ResponseUtil(False, 400).message_response("Hey! You are about to change your intake. Your mentor does not offer services in that month. Please remove the hired mentor in order to change the travelling info.")

                # if "university" not in params:
                #     params['university'] = ""

                # if "visible" not in params:
                #     params["visible"] = True
                # try:
                #     try:
                #         # params["arrival_dt"] = datetime.datetime.strptime(
                #         #     params["arrival_dt"], "%b %Y")
                #         # params["travelling_month"]
                #     except:
                #         params["arrival_dt"] = params["arrival_dt"]

                #     # if "departure_dt" in params:
                #     #     params["departure_dt"] = params["departure_dt"]
                #     # else:
                #     #     params["departure_dt"] = params[
                #     #         "arrival_dt"] + datetime.timedelta(days=30)

                # except Exception as e:
                #     print(e)
                # return ResponseUtil(False, 403).message_response("Invalid
                # datetime format")

                # if not Buddy.objects(
                #     service_at_city=params["arrival_city"],
                #     service_at_country=params["arrival_country"],
                #     # service_start__lte=params["arrival_dt"],
                #     # service_end__gte=params["departure_dt"] +
                #     #     datetime.timedelta(days=30)
                #     service_month=params[]
                # ):
                # return ResponseUtil(True, 404).message_response("not found")

                params["info_exists"] = True
                updated = self.__class__.objects(
                    user_id=user_id).update_one(**params)

                # print(updated)
                data = self.__class__.objects(user_id=user_id).to_json()

                updated = True

                if updated:
                    return ResponseUtil(True, 200).data_response(json.loads(data))
                else:
                    return ResponseUtil(False, 403).message_response("Failed to update")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Failed to update. Exception: " + str(e))

    def bookmark_profile(self, self_id=None, params=None):
        if self_id and params:
            
            user_objects = User.objects(_id=params["user_id"])
            
            if not user_objects:
                return ResponseUtil(False, 404).messages("user not found")

            if params["role"] == "buddy":
                try:
                    student_obj = self.__class__.objects.get(user_id=self_id)
                    student_obj.bookmarked_buddies = list(
                        set(student_obj.bookmarked_buddies + [params["user_id"]]))
                    student_obj.save()
                    
                    mentor_first_name = user_objects[0].first_name or ''
                    mentor_last_name = user_objects[0].last_name or ''
                    mentor_full_name = ' '.join([mentor_first_name, mentor_last_name])

                    return ResponseUtil(True, 200).message_response("{} is added to bookmarks".format(mentor_full_name))

                except Exception as e:
                    print(e)
                    app.logger.error(
                        "Exception while adding buddy to bookmarks: {}".format(e))
                    return ResponseUtil(False, 403).message_response("Failed to add bookmark")
            else:
                try:
                    student_obj = self.__class__.objects.get(user_id=self_id)
                    student_obj.bookmarked_students = list(
                        set(student_obj.bookmarked_students + [params["user_id"]]))
                    student_obj.save()
                    
                    student_first_name = user_objects[0].first_name or ''
                    student_last_name = user_objects[0].last_name or ''
                    student_full_name = ' '.join([student_first_name, student_last_name])

                    return ResponseUtil(True, 200).message_response("{} is added to bookmarks".format(student_full_name))

                except Exception as e:
                    print(e)
                    app.logger.error(
                        "Exception while adding student to bookmarks: {}".format(e))
                    return ResponseUtil(False, 403).message_response("Failed to add bookmark")
        else:
            return ResponseUtil(False, 403).messages("Failed to add bookmark")

    def get_bookmarked_profiles(self, self_id=None, service_model=None):
        if self_id:
            try:
                self_obj = self.__class__.objects.get(user_id=self_id)

                # if role == "student":
                bookmarked_profiles = self_obj.bookmarked_students
                students = json.loads(self.__class__.objects(
                    user_id__in=bookmarked_profiles).only(*C.STUDENT_INFO_FOR_BUDDY).to_json())
                user_profiles_students = self.get_students_meta_info(
                    students, self_id, "student", None)

                # elif role == "buddy":
                bookmarked_profiles = self_obj.bookmarked_buddies
                buddies = json.loads(Buddy.objects(user_id__in=bookmarked_profiles).only(
                    *C.BUDDY_INFO_FOR_STUDENT).to_json())
                user_profiles_buddies = Buddy().get_buddies_meta_info(
                    buddies, self_id, "student", service_model, None)
                # else:
                # return ResponseUtil(False, 403).message_response("Invalid
                # role")

                user_profiles = {
                    "students": user_profiles_students,
                    "buddies": user_profiles_buddies
                }

                if user_profiles:

                    return ResponseUtil(True, 200).data_response({"profiles": user_profiles})
                else:
                    return ResponseUtil(False, 404).message_response("No profiles bookmarked")

            except Exception as e:
                print(e)
                return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))
        else:
            return ResponseUtil(False, 403).message_response("Invalid request")

    def bookmark_profile_delete(self, self_id=None, params=None):
        if self_id and params:
            
            user_objects = User.objects(_id=params["user_id"])

            if not user_objects:
                return ResponseUtil(False, 404).messages("user not found")

            if params["role"] == "buddy":
                try:
                    student_obj = self.__class__.objects.get(user_id=self_id)
                    student_obj.bookmarked_buddies.remove(params["user_id"])
                    student_obj.save()

                    mentor_first_name = user_objects[0].first_name or ''
                    mentor_last_name = user_objects[0].last_name or ''
                    mentor_full_name = ' '.join([mentor_first_name, mentor_last_name])

                    return ResponseUtil(True, 200).message_response("{} is removed from bookmarks".format(mentor_full_name))

                except Exception as e:
                    print(e)
                    app.logger.error(
                        "Exception while removing buddy from bookmarks: {}".format(e))
                    return ResponseUtil(False, 403).message_response("Failed to remove mentor from bookmarks")
            else:
                try:
                    student_obj = self.__class__.objects.get(user_id=self_id)
                    student_obj.bookmarked_students.remove(params["user_id"])
                    student_obj.save()

                    student_first_name = user_objects[0].first_name or ''
                    student_last_name = user_objects[0].last_name or ''
                    student_full_name = ' '.join([student_first_name, student_last_name])

                    return ResponseUtil(True, 200).message_response("{} is removed from bookmarks".format(student_full_name))

                except Exception as e:
                    print(e)
                    app.logger.error(
                        "Exception while removing student from bookmarks: {}".format(e))
                    return ResponseUtil(False, 403).message_response("Failed to remove co-traveller from bookmarks")
        else:
            return ResponseUtil(False, 403).messages("Invalid request")

    def select_buddy(self, self_id, buddy_id, service_task_model, package_model):
        if self.__class__.objects(user_id=self_id):
            if Buddy.objects(user_id=buddy_id):

                self_obj = self.__class__.objects(
                    user_id=self_id, hired_buddy=None)

                if self_obj:

                    pkg_id = Buddy.objects.get(user_id=buddy_id).package_id
                    # add service tasks with student id and buddy id by default

                    try:
                        if ReportIssue.objects(student_id=self_id, buddy_id=buddy_id):
                            return ResponseUtil(False, 403).message_response("Uh ho! You cannot pick a mentor which is reported by you earlier.")

                        transaction_obj = Transaction.objects(
                            student_id=self_id)
                        if transaction_obj:
                            # transaction_obj.update_one(buddy_id=buddy_id)
                            transaction_json = json.loads(
                                transaction_obj.to_json())[0]
                            response_json = {
                                "is_transaction_complete": transaction_json["completed"]}
                            # if transaction_json["completed"]:
                            #     self_obj.update_one(hired_buddy=buddy_id)
                            #     service_task_model().add_default(self_id, buddy_id, pkg_id)

                        else:
                            response_json = {"is_transaction_complete": False}

                        packages = package_model().get_dict()
                        buddy_offers = []
                        for pkg in pkg_id:
                            packages[pkg]["package_id"] = pkg
                            buddy_offers.append(packages[pkg])
                        response_json['buddy_offers'] = buddy_offers
                        #response_json['packages'] = packages
                        response_json['buddy_id'] = buddy_id

                        return ResponseUtil(True, 200).data_response(response_json)

                    except Exception as e:
                        app.logger.error(
                            "Exception while getting a link {}".format(e))
                        return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))
                        
                    # return ResponseUtil(True, 200).message_response("Mentor hired successfully")
                else:
                    return ResponseUtil(False, 403).message_response("Hey! You have picked a Mentor already")
            else:
                return ResponseUtil(False, 404).message_response("Mentor profile not found")
        else:
            return ResponseUtil(False, 403).message_response("Please update your travelling details first")

    def deselect_buddy(self, self_id, params):

        self_obj = self.__class__.objects(
            user_id=self_id, hired_buddy__ne=None)

        if self_obj:

            # get registration_id for  buddy  from use doc
            if not ReportIssue.objects(student_id=self_id, buddy_id=self_obj[0].hired_buddy):
                try:
                    registration_id = User.objects.get(
                        _id=self_obj[0].hired_buddy).fcm_token

                    if registration_id:
                        student_name = User.objects.get(_id=self_id).first_name
                        display_response = PushNotification(registration_id).send_display_message({
                            "title": "You have been reported",
                            "message": "Hey! {} reported an issue against you.".format(student_name)
                            # 'message': json.dumps({
                            #     'type': 'REPORTED_ISSUE',
                            #     "message": "Hey! {} reported an issue against you.".format(student_name)
                            # })
                        })

                        # data_response = PushNotification(registration_id).send_data_message({
                        #     "title": "You have been reported",
                        #     "message": "Hey! {} reported an issue against you.".format(student_name)
                        # })

                        if display_response["success"] != 1:
                            app.logger.error(
                                "Failed to send push notification")

                except Exception as e:
                    app.logger.error(
                        "Exception while sending push notification: {}".format(e))

                ReportIssue.objects.create(
                    student_id=self_id,
                    buddy_id=self_obj[0].hired_buddy,
                    issue=params["issue"],
                    created=datetime.datetime.utcnow(),

                )
                # self_obj.update(hired_buddy=None,
                #                 student_availability=[], buddy_availability=[])

                # service_task_model.objects(student_id=self_id).delete()

                return ResponseUtil(True, 200).message_response("We are looking into the situation carefully to allow you to change your mentor. Please allow us two days")

            else:
                return ResponseUtil(False, 403).message_response("The mentor is already reported. We will get back to you on email.")

        else:
            return ResponseUtil(False, 404).message_response("Did not hire a mentor yet")


class Buddy(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()

    # service_start = fields.DateTimeField()
    # service_end = fields.DateTimeField()

    info_exists = fields.BooleanField(default=False)
    package_id = fields.ListField(default=[])
    is_verified = fields.BooleanField(defaut=False)
    is_bank_added = fields.BooleanField(defaut=False)
    is_docs_uploaded = fields.BooleanField(defaut=False)

    service_at_city = fields.StringField(defaut="")
    service_at_country = fields.StringField(defaut="")
    service_at_province = fields.StringField(defaut="")

    service_month = fields.ListField(defaut=[])
    service_year = fields.ListField(defaut=[])
    created = fields.DateTimeField()
    updated = fields.DateTimeField()
    is_package_selected = fields.BooleanField(defaut=True)

    meta = {"collection": "buddy"}

    def get_servicing_info(self, user_id=None):
        try:
            buddy_obj = json.loads(self.__class__.objects(user_id=user_id)
                                   .only(*C.BUDDY_INFO_FOR_BUDDY).to_json())[0]

            print(buddy_obj)
            return ResponseUtil(True, 200).data_response(buddy_obj)

        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response("User did not update servicing details yet")

    def get_all(self, user_id=None, n=None, filters=None, self_id=None, self_role=None, service_model=None, get_one=False, package_model=None, order_by=None, reverse_sort=False, search_keyword=None):

        if self_role == "student":
            try:

                if get_one:
                    buddies = json.loads(self.__class__.objects(
                        user_id=user_id
                    ).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())

                    try:
                        if buddies[0]["is_package_selected"]:
                            package_info = json.loads(package_model.objects(
                                _id=buddies[0]["package_id"]
                            ).only("name", "details", "amount", "services").to_json())

                            for k, v in package_info[0].items():
                                # print(k,v)
                                buddies[0][k] = v

                            buddy = buddies[0]
                            
                            packages = package_model().get_dict()
                            
                            buddy['packages'] = [packages[pkg_id] for pkg_id in buddy['package_id']]

                            if len(buddy["package_id"]) == 2:
                                buddy["name"] = "Small and Large"
                                for pkg_id in buddy["package_id"]:
                                    for k, v in packages[pkg_id].items():
                                        if k == 'name':
                                            continue
                                        if k == 'services' and len(v) == 2:
                                            continue
                                        buddy[k] = v
                            else:
                                pkg_id = buddy["package_id"][0]
                                for k, v in packages[pkg_id].items():
                                    buddy[k] = v

                            # print(buddies)
                    except Exception as e:
                        app.logger.error(
                            "Exception in user models while getting buddy package info {}".format(e))

                else:
                    self_obj = Student.objects.get(user_id=self_id)

                    # app.logger.error(json.loads(self_obj.to_json()))
                    query_params = {
                        "user_id__ne": str(self_obj.user_id),
                        "service_at_city": self_obj.arrival_city,
                        "service_at_country": self_obj.arrival_country,
                        "service_at_province": self_obj.arrival_province,
                        # service_start__lte:self_obj.arrival_dt,
                        # service_end__gte:self_obj.arrival_dt +
                        # datetime.timedelta(days:30)
                        "service_month__contains": self_obj.travelling_month,
                        "service_year__contains": self_obj.travelling_year,
                        "is_package_selected": True,
                        #"is_verified": True,
                        #"is_bank_added": True,
                        "info_exists": True,
                        #"is_docs_uploaded":True
                    }

                    
                    # filter results and show only currently active students
                    active_users_oids = json.loads(User.objects(
                        is_active=True).filter(is_deleted=False).only("_id").to_json())
                    
                    active_user_str_ids = []

                    for oid in active_users_oids:
                        active_user_str_ids.append(oid["_id"]["$oid"])

                    query_params["user_id__in"] = active_user_str_ids
                    
                    buddies = json.loads(self.__class__.objects(
                        **query_params).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())

                    if not buddies:
                        query_params.pop("service_at_city")

                        buddies = json.loads(self.__class__.objects(
                            **query_params).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())

                    if buddies:
                        packages = package_model().get_dict()

                        for buddy in buddies:
                            if buddy['is_package_selected']:
                                if len(buddy["package_id"]) == 2:
                                    buddy["name"] = "Small and Large"
                                    buddy["amount"] = "175 CAD/320 CAD"
                                    for pkg_id in buddy["package_id"]:
                                        for k, v in packages[pkg_id].items():
                                            if k == 'name' or k == 'amount':
                                                continue
                                            if k == 'services' and len(v) == 2:
                                                continue
                                            buddy[k] = v
                                else:
                                    pkg_id = buddy["package_id"][0]
                                    for k, v in packages[pkg_id].items():
                                        if k == 'amount':
                                            v = str(v)+' CAD'
                                        buddy[k] = v
                                buddy['packages'] = [packages[pkg_id] for pkg_id in buddy['package_id']]

                    else:
                        return ResponseUtil(False, 404).message_response("No mentors found")

            except Exception as e:
                app.logger.error("exception: {}".format(e))
                return ResponseUtil(False, 404).message_response("Something Went Wrong!")

        # elif self_role == "buddy":
        #     try:
        #         self_obj = Buddy.objects.get(user_id=self_id)

        #         buddies = json.loads(self.__class__.objects(
        #             user_id__ne=str(self_obj.user_id),
        #             service_at_city=self_obj.service_at_city,
        #             service_at_country=self_obj.service_at_country,
        #             # service_start__lte=self_obj.service_start,
        #             # service_end__gte=self_obj.service_start +
        #             # datetime.timedelta(days=30)
        #         ).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())
        #     except Exception as e:
        #         app.logger.error("exception: {}".format(e))
        # return ResponseUtil(False, 404).message_response("Please update your
        # servicing info first")

        else:
            buddies = json.loads(self.__class__.objects.all().to_json())

        if buddies:

            buddies = self.get_buddies_meta_info(
                buddies, self_id, self_role, service_model, search_keyword)

            if order_by:
                buddies = get_sorted(buddies, order_by, reverse_sort)

            if n:
                buddies = buddies[:n]

            return ResponseUtil(True, 200).data_response({"buddies": buddies})
        else:
            return ResponseUtil(False, 404).message_response("No mentors found")


    def get_alls(self, user_id=None, n=None, filters=None, self_id=None, self_role=None, service_model=None, get_one=False, package_model=None, order_by=None, reverse_sort=False, search_keyword=None, offset=None):

        if self_role == "student":
            try:

                if get_one:
                    buddies = json.loads(self.__class__.objects(
                        user_id=user_id
                    ).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())

                    try:
                        if buddies[0]["is_package_selected"]:
                            package_info = json.loads(package_model.objects(
                                _id=buddies[0]["package_id"]
                            ).only("name", "details", "amount", "services").to_json())

                            for k, v in package_info[0].items():
                                # print(k,v)
                                buddies[0][k] = v

                            buddy = buddies[0]
                            
                            packages = package_model().get_dict()
                            
                            buddy['packages'] = [packages[pkg_id] for pkg_id in buddy['package_id']]

                            if len(buddy["package_id"]) == 2:
                                buddy["name"] = "Small and Large"
                                for pkg_id in buddy["package_id"]:
                                    for k, v in packages[pkg_id].items():
                                        if k == 'name':
                                            continue
                                        if k == 'services' and len(v) == 2:
                                            continue
                                        buddy[k] = v
                            else:
                                pkg_id = buddy["package_id"][0]
                                for k, v in packages[pkg_id].items():
                                    buddy[k] = v

                            # print(buddies)
                    except Exception as e:
                        app.logger.error(
                            "Exception in user models while getting buddy package info {}".format(e))

                else:
                    self_obj = Student.objects.get(user_id=self_id)

                    # app.logger.error(json.loads(self_obj.to_json()))
                    query_params = {
                        "user_id__ne": str(self_obj.user_id),
                        "service_at_city": self_obj.arrival_city,
                        "service_at_country": self_obj.arrival_country,
                        # "service_at_province": self_obj.arrival_province,
                        # service_start__lte:self_obj.arrival_dt,
                        # service_end__gte:self_obj.arrival_dt +
                        # datetime.timedelta(days:30)
                        "service_month__contains": self_obj.travelling_month,
                        "service_year__contains": self_obj.travelling_year,
                        "is_package_selected": True,
                        #"is_verified": True,
                        #"is_bank_added": True,
                        "info_exists": True,
                        #"is_docs_uploaded":True
                    }

                    # filter results and show only currently active students
                    active_users_oids = json.loads(User.objects(
                        is_active=True).filter(is_deleted=False).only("_id").to_json())
                    
                    active_user_str_ids = []

                    for oid in active_users_oids:
                        active_user_str_ids.append(oid["_id"]["$oid"])

                    query_params["user_id__in"] = active_user_str_ids
                    offset = int(offset)
                    buddies = json.loads(self.__class__.objects(
                        **query_params).order_by('-created').limit(n).skip(offset).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())
                    
                    if offset == 0:
                        if len(buddies) < 5:
                            query_params.pop("service_at_city")
                            query_params['service_at_province'] = self_obj.arrival_province
                            buddies = json.loads(self.__class__.objects(
                            **query_params).order_by('-created').limit(n).skip(offset).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())

                    count = len(self.__class__.objects(**query_params))
                    if offset == 0:
                        if count < 5:
                            query_params.pop("service_at_city")
                            query_params['service_at_province'] = self_obj.arrival_province
                            count = len(self.__class__.objects(**query_params))

                    if offset == 0:
                        if not buddies:
                            query_params.pop("service_at_city")

                            buddies = json.loads(self.__class__.objects(
                                **query_params).order_by('-created').limit(n).skip(offset).only(*C.BUDDY_INFO_FOR_STUDENT).to_json())
                            count = len(self.__class__.objects(**query_params))
                            
                            if count < 5:
                                query_params.pop("service_at_city")
                                query_params['service_at_province'] = self_obj.arrival_province
                                count = len(self.__class__.objects(**query_params))

                    if buddies:
                        packages = package_model().get_dict()

                        for buddy in buddies:
                            if len(buddy["package_id"]) == 2:
                                buddy["name"] = "Small and Large"
                                buddy["amount"] = "175 CAD/320 CAD"
                                for pkg_id in buddy["package_id"]:
                                    for k, v in packages[pkg_id].items():
                                        if k == 'name' or k == 'amount':
                                            continue
                                        if k == 'services' and len(v) == 2:
                                            continue
                                        buddy[k] = v
                            else:
                                pkg_id = buddy["package_id"][0]
                                for k, v in packages[pkg_id].items():
                                    if k == 'amount':
                                        v = str(v)+' CAD'
                                    buddy[k] = v
                            buddy['packages'] = [packages[pkg_id] for pkg_id in buddy['package_id']]

                    else:
                        return ResponseUtil(False, 404).message_response("No mentors found")

            except Exception as e:
                app.logger.error("exception: {}".format(e))
                return ResponseUtil(False, 404).message_response("Something Went Wrong!")

        else:
            buddies = json.loads(self.__class__.objects.all().to_json())

        if buddies:

            buddies = self.get_buddies_meta_info(
                buddies, self_id, self_role, service_model, search_keyword)

            if order_by:
                buddies = get_sorted(buddies, order_by, reverse_sort)

            # if n:
            #     buddies = buddies[:n]

            return ResponseUtil(True, 200).data_response({"buddies": buddies, 'count': count})
        else:
            return ResponseUtil(False, 404).message_response("No mentors found")



    def get_buddies_meta_info(self, buddies, self_id, self_role, service_model, search_keyword):

        #print("buddies : ", buddies)

        try:
            if self_role == "student":
                bookmarked_buddies = Student.objects.get(
                    user_id=self_id).bookmarked_buddies
            else:
                bookmarked_buddies = []
        except Exception as e:
            print("Exception: " + str(e))
            bookmarked_buddies = []

        filter_buddies = []
        for buddy in buddies:
            try:

                # print(student)
                user_obj = json.loads(User.objects(
                    _id=buddy["user_id"]).exclude("password").to_json())

                buddy.update(user_obj[0])

                # buddy["_id"] = {"$oid": str(user_obj._id)}
                # buddy["image"] = user_obj.image
                # # buddy["full_name"] = " ".join(
                # #     [user_obj.first_name, user_obj.last_name])
                # buddy["first_name"] = user_obj.first_name
                # buddy["last_name"] = user_obj.last_name
                # buddy["city"] = user_obj.city
                # buddy["country"] = user_obj.country
                # buddy["sex"] = user_obj.sex
                # buddy["about"] = user_obj.about
                # buddy["contact"] = user_obj.contact
                # buddy["email"] = user_obj.email
                # buddy["home_address"] = user_obj.home_address
                # buddy["origin"] = user_obj.origin
                # buddy["university"] = user_obj.university
                # buddy["is_active"] = user_obj.is_active
                # buddy["date_of_birth"] = user_obj.date_of_birth if user_obj.date_of_birth == "" else str(
                #     int(user_obj.date_of_birth.timestamp()))
                # buddy["mother_tongue"] = user_obj.mother_tongue
                # buddy["languages"] = user_obj.languages
                # buddy["education"] = user_obj.education
                # buddy["employment"] = user_obj.employment
                # buddy["employment"] = user_obj.employment

                # buddy["city"] = user_obj.city
                # buddy["country"] = user_obj.country

                # add ratings to buddy info

                buddy_review = Review().get_ratings(buddy["user_id"])

                if buddy_review["status"]:
                    buddy["reviews_total"] = buddy_review[
                        "data"]["review_count"]
                    buddy["ratings"] = buddy_review["data"]["avg_rating"]
                else:
                    buddy["reviews_total"] = 0
                    buddy["ratings"] = 0

                if buddy["user_id"] in bookmarked_buddies:
                    buddy["bookmarked"] = True
                else:
                    buddy["bookmarked"] = False

                # add service and package info

                # buddy["services"] = list(service_model.objects(
                #     user_id=buddy["user_id"]).values_list("name"))

                #
                # buddy["package"] = "Full Package"
                # buddy["cost"] = 250

                buddy.pop("user_id")
                if search_keyword:
                    keyword = search_keyword.lower()
                    if keyword in buddy['university'].lower() or keyword in buddy['first_name'].lower() or keyword in buddy['last_name'].lower():
                        filter_buddies.append(buddy)

            except Exception as e:
                app.logger.error("Exception while getting buddies meta info for {} : {}".format(
                    buddy["user_id"], e))
        if search_keyword:
            return filter_buddies

        return buddies

    def get_dashboard_data(self, self_id, package_model):
        try:
            self_obj = self.__class__.objects.get(user_id=self_id)
        except Exception as e:
            err_msg = "Exception while getting buddy object: {}".format(e)
            # app.logger.error(err_msg)
            return ResponseUtil(False, 404).message_response("User does not exists")

        current_clients_ids = list(Student.objects(
            hired_buddy=self_id).values_list("user_id"))
        current_clients = json.loads(Student.objects(
            user_id__in=current_clients_ids).only(*C.STUDENT_INFO_FOR_BUDDY).to_json())
        current_clients = Student().get_students_meta_info(
            current_clients, self_id, "buddy", None)

        upcoming_clients_ids = list(Message.objects(
            receiver_id=self_id).values_list("sender_id"))
        upcoming_clients_ids = set(
            upcoming_clients_ids).difference(current_clients_ids)
        upcoming_clients = json.loads(Student.objects(
            user_id__in=upcoming_clients_ids).only(*C.STUDENT_INFO_FOR_BUDDY).to_json())

        if self_obj.package_id:
            try:
                # package_amount = package_model.objects.get(
                #     _id=self_obj.package_id).amount
                total_earning = 0
                transaction_objs = Transaction.objects(buddy_id=self_id)
                for obj in transaction_objs:
                    if obj.amount_to_buddy.isdigit():
                        total_earning = int(total_earning) + \
                            int(obj.amount_to_buddy)

                #total_earning = package_amount * len(current_clients_ids)
            except Exception as e:
                err_msg = "Exception while getting buddy's package info: {}".format(
                    e)
                app.logger.error(err_msg)
                app.logger.error("total_earning {}, amount_to_buddy {}".format(
                    total_earning, obj.amount_to_buddy))
                return ResponseUtil(False, 404).message_response("User package does not exists")
        else:
            total_earning = 0

        # print(upcoming_clients)

        upcoming_clients = Student().get_students_meta_info(
            upcoming_clients, self_id, "buddy", None)

        review_response = Review().get_ratings(self_id)
        if review_response["status"] and "data" in review_response:
            review_data = review_response["data"]
        else:
            review_data = None

        return ResponseUtil(True, 200).data_response({
            "students_served": len(current_clients),
            "total_earning": total_earning,
            "current_clients": current_clients,
            "upcoming_clients": upcoming_clients,
            "review": review_data,
            "is_contact_verified": User.objects.get(_id=self_id).is_contact_verified,
            "is_verified": self_obj.is_verified,
            "is_package_selected": self_obj.is_package_selected,
            "is_bank_added": self_obj.is_bank_added,
            "is_docs_uploaded":self_obj.is_docs_uploaded
        })

    def _update(self, params=None):
        try:
            user_id = params.pop("user_id")
            
            if "service_year" in params and params["service_year"]:
                if isinstance(params["service_year"], str):
                    params["service_year"] = [params["service_year"]]

            if not User.objects(_id=user_id):
                return ResponseUtil(False, 404).message_response("User does not exists")
            else:
                utc_now = datetime.datetime.utcnow()

                if not self.__class__.objects(user_id=user_id):
                    params["created"] = utc_now
                    params["upsert"] = True

                params["info_exists"] = True
                params["updated"] = utc_now

                updated = self.__class__.objects(
                    user_id=user_id).update_one(**params)

                if updated:
                    return ResponseUtil(True, 200).message_response("Congrats! You Availability has been saved")
                else:
                    return ResponseUtil(False, 403).message_response("Failed to update")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Failed to update. Exception: " + str(e))

    def add_default(self, user_id):
        if not self.__class__.objects(user_id=user_id):

            utc_now = datetime.datetime.utcnow()

            self.__class__.objects.create(
                user_id=user_id,
                # service_start=datetime.datetime.strptime("Jan 2018", "%b %Y"),
                # service_end=datetime.datetime.strptime("Jan 2019", "%b %Y"),
                service_at_city="",
                service_at_province="",
                service_at_country="",
                package_id=[],
                service_month=[],
                service_year=[],
                created=utc_now,
                updated=utc_now,
                info_exists=False,
                is_verified=False,
                is_package_selected=False,
                is_bank_added=False,
                is_docs_uploaded=False
            )

        else:
            return ResponseUtil(False, 403).message_response("Mentor already exists")


class TravelDocs(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()

    visa = fields.StringField()
    passport = fields.StringField()
    visa_img = fields.StringField()
    gic_cert = fields.StringField()

    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "travel_docs"}

    def add(self, user_id, params):
        try:

            utc_now = datetime.datetime.utcnow()

            if not self.__class__.objects(user_id=user_id):
                # create a document

                params.update({
                    "user_id": user_id,
                    "created": utc_now,
                    "updated": utc_now
                })

                print("create params", params)

                self.__class__.objects.create(**params)

                print("doc created")

            else:
                # update existing one
                print("update params", params)

                self.__class__.objects(user_id=user_id).update(**params)

                print("doc updated")

            # if self.__class__.objects(user_id=user_id, secondary_id__ne=None, police_verification__ne=None):
            #     Buddy.objects(user_id=user_id).update_one(is_verified=True)

            return ResponseUtil(True, 200).message_response("Travel document updated successfully")

        except Exception as e:
            app.logger.error(
                "Exception while updating verification_docs: {}".format(e))
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

    def get_one(self, user_id):

        try:
            # if Buddy.objects(user_id=user_id, is_verified=True):
            travel_docs = json.loads(self.__class__.objects(user_id=user_id).only(
                "visa", "passport", "visa_img", "gic_cert").to_json())[0]

            return ResponseUtil(True, 200).data_response(travel_docs)

            # else:
            #     return ResponseUtil(403, False).message_response("Please upload your docs")

        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response("Could not find files")


class VerificationDocs(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()

    primary_id = fields.StringField()
    secondary_id = fields.StringField()
    police_verification = fields.StringField()

    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "verification_docs"}

    def add(self, user_id, params):
        try:

            utc_now = datetime.datetime.utcnow()

            if not self.__class__.objects(user_id=user_id):
                # create a document

                params.update({
                    "user_id": user_id,
                    "created": utc_now,
                    "updated": utc_now
                })

                print("create params", params)

                self.__class__.objects.create(**params)

                print("doc created")

            else:
                # update existing one
                print("update params", params)

                self.__class__.objects(user_id=user_id).update(**params)

                print("doc updated")

            if self.__class__.objects(user_id=user_id):
                Buddy.objects(user_id=user_id).update_one(is_docs_uploaded=True)

            return ResponseUtil(True, 200).message_response("Required documents have been updated")

        except Exception as e:
            app.logger.error(
                "Exception while updating verification_docs: {}".format(e))
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

    def get_one(self, user_id):

        try:
            if Buddy.objects(user_id=user_id, is_docs_uploaded=True):
                verification_docs = json.loads(self.__class__.objects(user_id=user_id).only(
                    "primary_id", "secondary_id", "police_verification").to_json())[0]

                return ResponseUtil(True, 200).data_response(verification_docs)

            else:
                return ResponseUtil(False, 403).message_response("Please upload your docs")

        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response("Could not find files")


class BankDetails(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()
    transit_number = fields.StringField()
    institution_number = fields.StringField()
    bank_name = fields.StringField()
    account_number = fields.StringField()
    account_holder_name = fields.StringField()
    # address = fields.StringField()
    # city = fields.StringField()

    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "bank_details"}

    def add_details(self, user_id, params):

        try:

            utc_now = datetime.datetime.utcnow()

            if not self.__class__.objects(user_id=user_id):
                # create a document

                params.update({
                    "user_id": user_id,
                    "created": utc_now,
                    "updated": utc_now
                })

                self.__class__.objects.create(**params)

                try:
                    Buddy.objects(user_id=user_id).update(is_bank_added=True)

                except Exception as e:
                    app.logger.error(
                        "Error updating is_bank_added for buddy_id {}".format(user_id))

            else:
                params.update({"updated": utc_now})
                self.__class__.objects(user_id=user_id).update(**params)

            return ResponseUtil(True, 200).message_response("Banking details have been saved")

        except Exception as e:
            app.logger.error(
                "Exception while updating bank details: {}".format(e))
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

    def get_one(self, user_id):

        try:

            bank_details_obj = self.__class__.objects(user_id=user_id)
            if bank_details_obj:
                bank_details = json.loads(bank_details_obj.only(
                    "user_id", "institution_number", "transit_number", "bank_name", "account_number", "account_holder_name").to_json())[0]
                return ResponseUtil(True, 200).json_data_response({"bank_details": bank_details})

            else:
                return ResponseUtil(False, 403).message_response("User has not added the details yet.")

        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))


class Message(Document):
    _id = fields.ObjectIdField()

    sender_id = fields.StringField()
    receiver_id = fields.StringField()

    message = fields.StringField()

    is_deleted = fields.BooleanField(default=False)
    sent_on = fields.DateTimeField()

    sender_role = fields.StringField()
    receiver_role = fields.StringField()

    def add_one(self, params=None):
        if not params:
            return ResponseUtil(False, 403).message_response("Invalid request")

        elif params["sender_id"] == params["receiver_id"]:
            return ResponseUtil(False, 403).message_response("You can not send a message to yourself")

        else:
            try:
                utc_now = datetime.datetime.utcnow()

                if Block.objects(self_id=params["receiver_id"], user_id=params["sender_id"], is_blocked=True):
                    return ResponseUtil(False, 403).json_data_response({
                        "message": "Hmm! This message can not be delivered."
                    })

                msg_obj = self.__class__.objects.create(
                    sender_id=params["sender_id"],
                    receiver_id=params["receiver_id"],
                    message=str(params["message"]),
                    sent_on=utc_now,
                    sender_role=params["sender_role"].lower(),
                    receiver_role=params["receiver_role"].lower()
                )

                message_json = json.loads(msg_obj.to_json())
                message_json["_id"] = msg_obj.pk
                message_json.pop("is_deleted")

                if not LastSeen.objects(self_id=params["receiver_id"], user_id=params["sender_id"]):
                    LastSeen.objects.create(
                        self_id=params["receiver_id"], user_id=params["sender_id"])

                # send push notification
                try:
                    sender_name = User.objects.get(
                        _id=params["sender_id"]).first_name
                    registration_id = User.objects.get(
                        _id=params["receiver_id"]).fcm_token
                    params = {
                        "title": "{} sent a message".format(sender_name),
                        "message": str(params["message"])
                        # 'message': json.dumps({
                        #     'type': 'NEW_MESSAGE',
                        #     'sender_id': params['sender_id'],
                        #     "message": str(params["message"])
                        # })
                    }

                    display_response = PushNotification(
                        registration_id).send_display_message(params)
                    # data_response = PushNotification(
                    #     registration_id).send_data_message(params)

                    if display_response["success"] != 1:
                        app.logger.error("Failed to send push notification")
                    else:
                        print("\n\npushnotification sent\n\n")

                except Exception as e:
                    app.logger.error(
                        "Exception while sending push notification: {}".format(e))

                return ResponseUtil(True, 200).json_data_response({
                    "message": message_json
                })

            except Exception as e:
                print(e)
                return ResponseUtil(False, 403).message_response("Failed to add message. Exception: " + str(e))

    def get_all(self, params=None):
        if not params:
            return ResponseUtil(False, 403).message_response("Invalid request")
        else:
            messages = json.loads(self.__class__.objects(sender_id__in=params.values(), receiver_id__in=params.values(
            )).only("_id", "message", "sender_id", "receiver_id", "sent_on").order_by('sent_on').to_json())

            if params["offset"]:
                offset = int(params["offset"])
                count = 10
                if offset:
                    messages = messages[-count-offset:-offset]
                else:
                    messages = messages[-count:]

            # update last seen
            LastSeen()._update(params)

            try:
                block_obj = Block.objects.get(
                    self_id=params["sender_id"], user_id=params["receiver_id"])
                blocked_by_me = block_obj.is_blocked

            except Exception as e:
                blocked_by_me = False

            try:
                block_obj = Block.objects.get(
                    self_id=params["receiver_id"], user_id=params["sender_id"])
                has_blocked_me = block_obj.is_blocked

            except Exception as e:
                has_blocked_me = False

            return ResponseUtil(True, 200).json_data_response({
                "messages": messages,
                "blocked_by_me": blocked_by_me,
                "has_blocked_me": has_blocked_me
            })

    def get_sorted(self, list_of_dict, var, reverse):
        return sorted(list_of_dict, key=lambda k: k[var], reverse=True)

    def get_last_messages(self, messages):
        response, user_ids = [], []
        for message in messages:
            try:
                index = user_ids.index(message["user_id"])
                # print(message["sent_on"], response[index]["last_message_time"])
                if message["sent_on"] > response[index]["last_message_time"]:
                    response[index]["last_message_time"] = message["sent_on"]
                    response[index]["last_message"] = message["message"]
                    response[index]["role"] = message["role"]

            except ValueError:
                user_ids.append(message["user_id"])
                response.append({"user_id": message["user_id"],
                                 "last_message": message["message"],
                                 "last_message_time": message["sent_on"],
                                 "role": message["role"]
                                 })

        response = self.get_sorted(response, 'last_message_time', reverse=True)
        return response

    def modulate(self, messages, message_type):
        ret = []

        for message in messages:
            if message_type == "sent":
                ret.append({
                    "user_id": message["receiver_id"],
                    "message": message["message"],
                    "sent_on": message["sent_on"]["$date"],
                    "role": message["receiver_role"]
                })
            else:
                ret.append({
                    "user_id": message["sender_id"],
                    "message": message["message"],
                    "sent_on": message["sent_on"]["$date"],
                    "role": message["sender_role"]
                })

        return ret

    def get_unread_count(self, self_id, user_id):
        try:
            last_seen_dt = LastSeen.objects.get(
                self_id=self_id, user_id=user_id).last_seen_dt

            unread = self.__class__.objects(
                receiver_id=self_id, sender_id=user_id, sent_on__gt=last_seen_dt).count()

            return unread
        except Exception as e:
            print(e)
            return 0

    def add_user_information(self, self_id, response):
        ret = {"buddy": [], "student": []}
        for chat in response:
            user_object = User.objects.get(_id=chat["user_id"])
            chat["image"] = user_object["image"]
            # chat["full_name"] = " ".join(
            #     [user_object["first_name"], user_object["last_name"]])
            chat["first_name"] = user_object["first_name"]
            chat["last_name"] = user_object["last_name"]
            chat["unread"] = self.get_unread_count(self_id, chat["user_id"])
            role = chat.pop("role")
            ret[role].append(chat)

        return ret

    def get_chat_list(self, params=None):
        if not params:
            return ResponseUtil(False, 403).message_response("Invalid request")

        else:

            messages = json.loads(self.__class__.objects(
                sender_id=params['user_id']).to_json())
            messages_sent = self.modulate(messages, "sent")

            messages = json.loads(self.__class__.objects(
                receiver_id=params['user_id']).to_json())
            messages_received = self.modulate(messages, "received")

            messages = messages_sent + messages_received
            if len(messages) == 0:
                return ResponseUtil(False, 403).json_message_response("No messages")

            else:

                response = self.get_last_messages(messages)
                response = self.add_user_information(
                    params["user_id"], response)
                return ResponseUtil(True, 200).json_data_response({
                    "chat_list": response
                })

    def get_message_admin(self, params):
        messages = json.loads(self.__class__.objects(**params).to_json())
        return messages

    def delete_messages(self, params=None):
        if not params:
            return ResponseUtil(False, 200).json_data_response({
                "message": "No params provided to delete"
            })
        try:
            # index = 0
            # for id in params['deleteIds']:
            #     params[index] = ObjectId(id)
            #     index = index = 1
            query_params = {}
            query_params["_id__in"] = params['deleteIds']
            deletedIds = self.__class__.objects(**query_params).delete()
            if deletedIds:
                return ResponseUtil(True, 200).json_data_response({
                    "message": "Message deleted"
                })
            else:
                return ResponseUtil(False, 200).json_data_response({
                    "message": "Could not perform delete"
                })
        except Exception as e:
            app.logger.error("Exception while delete chat: {}".format(e))
            return ResponseUtil(False, 200).json_data_response({
                "message": "Exception in delete chat"
            })
        




class LastSeen(Document):
    _id = fields.ObjectIdField()

    self_id = fields.ObjectIdField()
    user_id = fields.ObjectIdField()

    last_seen_dt = fields.DateTimeField(
        default=datetime.datetime(2018, 3, 7, 10, 52, 27, 936284))

    def _update(self, params):
        try:
            self.__class__.objects(
                self_id=params["sender_id"],
                user_id=params["receiver_id"]
            ).update(
                last_seen_dt=datetime.datetime.utcnow(),
                upsert=True
            )

            return True
        except Exception as e:
            print(e)
            return False


class Block(Document):
    _id = fields.ObjectIdField()

    self_id = fields.ObjectIdField()
    user_id = fields.ObjectIdField()

    is_blocked = fields.BooleanField(default=False)
    
    meta = {"collection": "blocks"}

    def _update(self, params):
        try:
            self.__class__.objects(
                self_id=params["receiver_id"],
                user_id=params["sender_id"]
            ).update(
                is_blocked=params["is_blocked"],
                upsert=True
            )

            return True
        except Exception as e:
            print(e)
            return False


class Review(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()
    writer = fields.StringField()

    rating = fields.FloatField()
    review = fields.StringField()

    created = fields.DateTimeField()

    def add_one(self, params=None):
        try:
            if not self.__class__.objects(writer=params["writer"], user_id=params["user_id"]):

                required_fields = ["user_id", "writer", "rating", "review"]

                if all(x in params for x in required_fields):
                    params["created"] = datetime.datetime.utcnow()

                    review_obj = self.__class__.objects.create(**params)

                    return ResponseUtil(True, 200).message_response("Review added successfully")

                else:
                    return ResponseUtil(False, 403).message_response("Invalid/Incomplete json request")

            else:
                return ResponseUtil(False, 403).message_response("You can not add a review again for same buddy")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Failed to add a review. Exception: " + str(e))

    def get_all(self, params=None):
        pass

    def get_ratings(self, user_id):
        user_reviews = self.__class__.objects(user_id=user_id)

        if user_reviews:
            avg_rating = user_reviews.average("rating")
            review_count = user_reviews.count()

            return ResponseUtil(True, 200).data_response({
                "avg_rating": avg_rating,
                "review_count": review_count
            })
        else:
            return ResponseUtil(False, 404).message_response("No reviews for current user")


class TimelineFeed(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()
    user_image = fields.StringField()
    full_name = fields.StringField(default=None)

    title = fields.StringField(default="")
    story = fields.StringField(default="")
    image = fields.StringField(default="")

    visible = fields.BooleanField(default=False)
    created = fields.DateTimeField(defaut=datetime.datetime.utcnow())

    meta = {"collection": "timeline_feed"}

    def add_one(self, params):
        try:
            utc_now = datetime.datetime.utcnow()

            try:
                usr_obj = User.objects.get(_id=params["user_id"])
                user_full_name = " ".join(
                    [usr_obj.first_name, usr_obj.last_name])
                user_image = usr_obj.image
            except Exception as e:
                print(e)
                user_full_name = "Unnamed"
                user_image = ""

            obj = self.__class__.objects.create(
                user_id=params["user_id"],
                full_name=user_full_name,
                user_image=user_image,
                title=params["heading"],
                story=params["description"],
                image=params["image"],
                created=utc_now
            )

            response_data = {
                "_id": {"$oid": str(obj.pk)},
                "user_id": params["user_id"],
                "full_name": user_full_name,
                "heading": params["heading"],
                "description": params["description"],
                "image": params["image"],
                "timestamp": {"$date": int(utc_now.timestamp() * 1000)}
            }

            # make story visible
            # self.__class__.objects(_id=str(obj.pk)).update_one(visible=True)f

            return ResponseUtil(True, 200).data_response(response_data)

        except Exception as e:
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

    def get_all(self, params, count=0, order_by=None, reverse_sort=False):

        user_images_with_oid = json.loads(
            User.objects.only("_id", "image").to_json())
        user_images_with_str_id = {}

        for user_image_data in user_images_with_oid:
            user_images_with_str_id[user_image_data["_id"]
                                    ["$oid"]] = user_image_data["image"]

        feed_data = json.loads(self.__class__.objects(
            **params).to_json())

        if count:
            feed_data = feed_data[:count]

        if feed_data:

            for feed in feed_data:
                feed["user_image"] = user_images_with_str_id[feed["user_id"]]

            feed_data = get_sorted(feed_data, "created", reverse_sort=True)

            return ResponseUtil(True, 200).data_response(feed_data)

        else:
            return ResponseUtil(False, 404).message_response("No posts found")

    def _delete(self, params):
        try:
            deleted = self.__class__.objects(_id=params["id"]).delete()

            if deleted:
                return ResponseUtil(True, 200).message_response("Story removed from timeline successfully")
            else:
                return ResponseUtil(False, 404).message_response("Post not found")

        except Exception as e:
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

    # def get_by_visible(self, visible):
    #     try:
    #         feed_by_visible = self.__class__.objects(visible=visible).to_json()
    #         return feed_by_visible

    #     except Exception as e:
    # return ResponseUtil(False, 403).message_response("Exception occurred
    # while getting the feed: {}".format(e))

    # def get_by_visible(self, user_id, visible=None):
    #     try:
    #         if visible is None:
    #             params = {"user_id": user_id}
    #         else:
    #             params = {"user_id": user_id, "visible": visible}

    #         feed_by_userid = self.__class__.objects(**params).to_json()
    #         return feed_by_visible

    #     except Exception as e:
    # return ResponseUtil(False, 403).message_response("Exception occurred
    # while getting the feed: {}".format(e))

    def change_visible(self, feed_id, visible):
        try:
            doc = self.__class__.objects.get(_id=feed_id)
            self.__class__.objects(_id=feed_id).update(
                visible=visible)
            return ResponseUtil(True, 202).message_response("Successfully updated")

        except Exception as e:
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))


class EventManager(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()

    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "event_mgr"}


class Transaction(Document):

    _id = fields.ObjectIdField()
    student_id = fields.StringField()
    buddy_id = fields.StringField()
    initiated = fields.BooleanField(default=True)
    completed = fields.BooleanField(default=False)
    # student bank account information

    # student to kh payment related fields
    amount_to_kh = fields.StringField()
    amount_to_kh_inr = fields.StringField()
    purpose = fields.StringField(default="")
    url = fields.StringField(default="")
    payment_request_id = fields.StringField(default="")
    payment_id = fields.StringField(default="")
    status_to_kh = fields.StringField(default="Pending")
    payment_gateway = fields.StringField(default="")
    package_id = fields.StringField(default="")
    payment_type = fields.StringField(default="Indian")

    # kh to buddy related field
    amount_to_buddy = fields.StringField()
    amount_paid_to_buddy = fields.BooleanField(default=False)
    remarks = fields.StringField()
    # status_to_buddy = fields.BooleanField(default=False)

    payment_to_edapt_time = fields.DateTimeField()
    payment_to_buddy_time = fields.DateTimeField()
    created = fields.DateTimeField()

    def add_one(self, params):
        pass

    def update(self, params):
        pass

    def get_status(self, student_id=None):
        try:
            status = False
            payment_obj = self.__class__.objects.get(student_id=student_id)
            if payment_obj:
                status = payment_obj.status_to_kh

            return ResponseUtil(True, 200).json_data_response({"payment_status": status})
        except Exception as e:
            app.logger.error(
                "Exception while getting payment status. {}".format(e))
            return ResponseUtil(False, 403).message_response("Exception while getting payment status. {}".format(e))

    def get_all(self, buddy_paid=None, status_to_kh=None):
        try:
            if buddy_paid is not None and status_to_kh is not None:
                transactions = json.loads(self.__class__.objects(
                    amount_paid_to_buddy=buddy_paid, status_to_kh=status_to_kh).to_json())

            elif buddy_paid is None and status_to_kh is None:
                transactions = json.loads(
                    self.__class__.objects.all().to_json())

            else:
                return ResponseUtil(False, 403).message_response('Buddy_paid or success field missing')

            if transactions:
                return ResponseUtil(True, 200).data_response({"transactions": transactions})
            else:
                return ResponseUtil(False, 403).message_response("No matching transaction found")

        except Exception as e:
            app.logger.error(
                "Exception while getting all the transaction. {}".format(e))
            return ResponseUtil(False, 403).message_response("Exception while getting the transaction. {}".format(e))

    def get_one(self, transaction_id):
        try:
            transaction = json.loads(self.__class__.objects(
                _id=transaction_id).to_json())[0]
            return ResponseUtil(True, 200).json_data_response({"transaction": transaction})
        except Exception as e:
            app.logger.error(
                "Exception while getting one transaction. {}".format(e))
            return ResponseUtil(False, 403).message_response("Exception while getting one transaction. {}".format(e))

    def get_link(self, params, package_model):
        try:
            pkg_obj = package_model.objects.get(_id=params["package_id"])
            amount = str(pkg_obj.amount)
            purpose = "{} Package Fees".format(pkg_obj.name)
            print(purpose)
            resonse_payment, amount_inr = PaymentInstamojo().payment_request(
                amount, purpose, convert_to_inr=True)

            student_id = params.pop("student_id")
            if resonse_payment["success"]:
                params["amount_to_kh"] = amount
                params["amount_to_kh_inr"] = amount_inr
                params["purpose"] = purpose
                params["url"] = resonse_payment['payment_request']['longurl']
                params["payment_request_id"] = resonse_payment['payment_request']['id']
                #params["buddy_id"] = params["buddy_id"]
                params["payment_gateway"] = "Instamojo"
                params["completed"] = False
                params["initiated"] = True
                params["amount_paid_to_buddy"] = False

                if params["amount_to_kh"] == '320':
                    params['amount_to_buddy'] = '275'
                elif params["amount_to_kh"] == '175':
                    params['amount_to_buddy'] = '150'
                else:
                    params['amount_to_buddy'] = "0"

                params["status_to_kh"] = 'Pending'
                params["created"] = datetime.datetime.utcnow()

                try:
                    if self.__class__.objects(student_id=student_id, buddy_id=params["buddy_id"]):
                        buddy_id = params.pop("buddy_id")
                        self.__class__.objects(
                            student_id=student_id, buddy_id=buddy_id).update(**params)
                    else:
                        params["student_id"] = student_id
                        self.__class__.objects(student_id=student_id).create(**params)

                    return ResponseUtil(True, 200).data_response({
                        "link": resonse_payment['payment_request']['longurl']
                    })
            
                except Exception as e:
                    app.logger.error("Error while generating a link {}".format(e))
                    return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))

            else:
                return ResponseUtil(False, 403).message_response("Failed to create link. Please try again")

        except Exception as e:
            app.logger.error(
                "Exception while paying to the account. {}".format(e))
            return ResponseUtil(False, 403).message_response(app.config.get('ERROR_MESSAGE'))


class ReportIssue(Document):

    _id = fields.ObjectIdField()
    student_id = fields.StringField()
    buddy_id = fields.StringField()
    issue = fields.StringField(default="")
    created = fields.DateTimeField()
    issue_resolved = fields.BooleanField(default=False)
    updated = fields.DateTimeField()
    remarks = fields.StringField(default="")
    remove_hired_buddy = fields.BooleanField(default=False)

    # def add_one(self, params):
    #     self.__class__.objects.create(params)
    #     return
