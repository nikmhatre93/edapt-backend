from app import app
import unittest
import tempfile
import xmlrunner,json
from mongoengine import connect
from app.tests.test_helper import register, login, decode


class KHTestBuddy(unittest.TestCase):

	"""
	Unittest the endpoints and to be used in continous integration with a jenkins server for a buddy
	"""

	def setUp(self):
		self.db_client = connect(app.config.get('MONGODB_DATABASE'), username=app.config.get('MONGODB_USERNAME'), password=app.config.get('MONGODB_PASSWORD'))
		self.app = app.test_client()
		register_response = register("testbuddysetup@tweeny.in", "12345", "buddy")
		
		if register_response['code'] == 200 or register_response['code'] == 403:
			login_response = login("testbuddysetup@tweeny.in", "12345", "buddy")

			if login_response['code'] == 200:
				self.headers = login_response['data']['access_token']

	def test_get_dashboard(self):
		response = decode(self.app.get('/dashboard', headers=self.headers))
		
		if response['code'] == 200:
			assert True

		else:
			assert False


	def tearDown(self):
		self.db_client.close()

if __name__ == '__main__':
	unittest.main()