from app import app
import unittest
import tempfile
import xmlrunner,json
from mongoengine import connect
from app.tests.test_helper import login, register, decode

class KHTestStudent(unittest.TestCase):

	"""
	Unittest the endpoints and to be used in continous integration with a jenkins server for a student
	"""

	def setUp(self):
		self.db_client = connect(app.config.get('MONGODB_DATABASE'), username=app.config.get('MONGODB_USERNAME'), password=app.config.get('MONGODB_PASSWORD'))
		self.app = app.test_client()
		self.buddy_email = "testbuddysetup@tweeny.in"
		self.buddy_pwd = "12345"
		self.student_email = "teststudentsetup@tweeny.in"
		self.student_password = "12345"
		register_response = register(self.student_email, self.student_password, "student")

		if register_response['code'] == 200 or register_response['code'] == 403:
			login_response = login("teststudentsetup@tweeny.in", "12345", "student")
			
			if login_response['code'] == 200:
				access_token = login_response['data']['access_token']
				self.headers = {'Authorization':access_token, "content-type":"application/json"}
				self.user_id = login_response['data']['user']['user_id']
				get_buddies_response = self.get_buddies()

				if get_buddies_response['code'] == 404:
					self.update_user_info()
					self.update_travel_info()
					get_buddies_response = self.get_buddies()

				if len(get_buddies_response['data']['buddies']) == 0:
					self.register_a_buddy()
					self.buddy_id = self.get_buddies()['data']['buddies'][0]['_id']['$oid']
				
				else:
					login_response = login(self.buddy_email, self.buddy_pwd, 'buddy')
					self.buddy_headers = {'Authorization':login_response['data']['access_token'], "content-type":"application/json"}
					self.buddy_id = login_response['data']['user']['user_id']
					

	def register_a_buddy():
		register_response = register(self.buddy_email, self.buddy_pwd, "buddy")
		return

	def test_get_account_info(self):
		response = self.app.get('/account', headers=self.headers)
		response = json.loads(response.data.decode('utf-8'))

		if response['code'] == 200:
			assert True
		else:
			assert False

	def update_user_info(self):
		response = decode(self.app.post('/account', data=json.dumps({
				"first_name": "Vedprakash",
				"last_name": "Upraity",
				"image": "https://www.tweeny.in/static/img/new-logo-inverted-512.png",
				"city": "Pune",
				"country": "India",
				"university": "MIT",
				"home_address": "",
				"origin": "India",
				"date_of_birth": "1995-10-12T18:30:00.000Z",
				"mother_tongue": "Hindi",
				"languages": ["Hindi", "English"],
				"employment": "Student"
			}), headers=self.headers))
		
		if response['code'] == 200:
			return

		else:
			assert False

	def update_travel_info(self):
		response = decode(self.app.post('/student', data=json.dumps({
				"arrival_city": "Toronto",
				"arrival_country": "Canada",
				"arrival_dt": "Mar 2018"
				}), headers=self.headers))
		return

	def get_buddies(self):
		response = decode(self.app.get('/buddy', headers=self.headers))
		return response

	def test_bookmark_buddies(self):
		response = decode(self.app.post('/bookmark/profile', data=json.dumps({
			"user_id": self.buddy_id,
			"role": "buddy"
			}), headers=self.headers))

		if response['code'] == 200:
			response = decode(self.app.delete('/bookmark/profile', data=json.dumps({
				"user_id": self.buddy_id,
				"role": "buddy"
				}), headers=self.headers))
			
			assert True

		elif response['code'] == 403:
			assert False

	def test_fire_buddy(self):
		hire_response = decode(self.app.post('/mybuddy', data=json.dumps({
			"buddy_id" : self.buddy_id
			}), headers=self.headers))
		
		if hire_response['code'] == 200:
			fire_response = decode(self.app.delete('/mybuddy', headers=self.headers))
			if fire_response['code'] == 200:
				assert True

		else:
			assert False


	def test_chat_list(self):
		message_sent = "dear fellow buddy"
		response1 = decode(self.app.post('/message', data=json.dumps({
			"receiver_id": self.buddy_id,
			"message" : message_sent,
			'sender_role':'student',
			'receiver_role':'buddy'
			}), headers=self.headers))

		if response1['code'] == 200:
			response2 = decode(self.app.get('/chat', headers=self.buddy_headers))
	
			if response2['code'] == 200:
				student_chat = response2['data']['chat_list']['student']
				for last_message in student_chat:
					if last_message['user_id'] == self.user_id and last_message['unread']!= 0:
						response3 = decode(self.app.get('/message/'+self.user_id, headers=self.buddy_headers))
						
						if response3['code'] == 200:
							if response3['data']['messages'][-1]["message"] == message_sent:
								assert True

			elif response2['code'] == 403:
				assert False

	
	def tearDown(self):
		self.db_client.close()

if __name__ == '__main__':
	unittest.main()
	#testRunner=xmlrunner.XMLTestRunner(output="./python_unittests_xml")
