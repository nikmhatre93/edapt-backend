from flask import Flask
import os
from mongoengine import connect, Document
from werkzeug.contrib.fixers import ProxyFix
from flask_cors import CORS
import pymongo


app = Flask(__name__, static_url_path="/static")
CORS(app)

environment = os.environ.get('prod_env', 'development')

""" Setting up the environment from the mainconf """

if environment == 'production':
    app.config.from_object('conf.mainconf.ProductionConfig')
else:
    app.config.from_object('conf.mainconf.DevelopmentConfig')

""" Initiating the logger"""
logger = app.logger
logger.setLevel('DEBUG')

""" Initiating mongodb db object """

try:
    db = connect(
        # 'chatapp'
        host='mongodb://localhost:27017/chatapp'    
        )
    # db = connect(, , password=,
    # 			ServerSelectionTimeoutMS=1000)
    print(db)
    dbnames = db.database_names()

except pymongo.errors.ServerSelectionTimeoutError as err:
    print(err)

"""Set the app secret to use sessions """
app.secret_key = 'HRU5$e#/3yX!w@#tRsN!jmN]LWX/,?RT'

from app import api
